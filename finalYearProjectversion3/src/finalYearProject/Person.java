package finalYearProject;

import java.util.ArrayList;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Person {
	private int posX;
	private int posY;
	private ArrayList<String> lines; // what the person will say when talked to
	
	public Person(int posX, int posY){
		this.posX = posX;
		this.posY = posY;
		lines = new ArrayList<String>();
	}
	
	/**
	 * returns the person's X position
	 * @return int 
	 */
	public int getPosX() {
		return posX;
	}


	/**
	 * sets the person's X Position 
	 * @param posX of type int
	 */
	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	/**
	 * returns the person's Y position
	 * @return int 
	 */
	public int getPosY() {
		return posY;
	}

	/**
	 * sets the person's Y Position 
	 * @param posY of type int
	 */
	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	/**
	 * returns the person's lines
	 * @return ArrayList 
	 */
	public ArrayList<String> getLines(){
		return lines;
		
	}
	
	/**
	 * adds a new item to the person ArrayList 
	 * @param neoLine of type String 
	 */
	public void addLine(String neoLine){
		lines.add(neoLine);
	}
	
	/**
	 * returns the i'th line 
	 * @param i of type int
	 * @return String
	 */
	public String getline(int i){
		return lines.get(i);
	}
}
