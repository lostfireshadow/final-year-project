package finalYearProject;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Clothes extends Item{
	private String describe;
	private int addedDefence;
	private String type;

	public Clothes(String deName, int deCost, String describe, int deAddedDefence, String deType) {
		super(deName, deCost);
		this.describe = describe;
		this.addedDefence = deAddedDefence;
		this.type = deType;
		
	}
	
	
	/**
	 * return the clothe's's description         
	 * @return String 
	 */
	public String getDescribe() {
		return describe;
	}
	
	/**
	 * sets the clothe's's description         
	 * @param describe of type String 
	 */
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	
	/**
	 * sets the clothe's additional defense          
	 * @param newDefence of type int
	 */
	public void setAddedDefence(int newDefence){
		this.addedDefence = newDefence; 
		
	}
	
	/**
	 * return the sets of clothe's additional defense        
	 * @return int
	 */
	public int getAD(){
		return addedDefence;
	}
	
	/**
	 * return the type of armour the clothes are        
	 * @return String 
	 */
	public String getType(){
		return type;
	}
	
	/**
	 * sets the type of armour the clothes are       
	 * @param deType of type String
	 */
	public void setType(String deType){
		this.type = deType;
	}

}
