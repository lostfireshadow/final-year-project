package finalYearProject;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class MedicalItem extends Item {
	private boolean health; // if the item heals or restored Mana
	private int amount; /// the amount the item restores

	public MedicalItem(String deName, int decost, boolean isHealth, int deAmount) {
		super(deName, decost);
		this.health = isHealth;
		this.amount = deAmount;
	}
	
	/**
	 * returns if item heals health
	 * @return boolean
	 */
	public boolean getIsHealth() {
		return health;
	}
	
	/**
	 * set  if item heals health
	 * @param health of type boolean
	 */
	public void setHealth(boolean health) {
		this.health = health;
	}
    
	/**
	 * returns the amount it recovers health or mana by
	 * @return int
	 */
	public int getAmount() {
		return amount;
	}
    
	/**
	 * sets the amount it recovers health or mana by
	 * @param amount of type int
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

}
