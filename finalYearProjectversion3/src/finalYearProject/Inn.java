package finalYearProject;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Inn extends Building {
	private int cost;

	public Inn(int posX, int posY, int deCost) {
		super(posX, posY);
		this.cost = deCost;
	}
	
	/**
	 * returns the inn's cost per stay
	 * @return int
	 */
	public int getCost(){
		return cost;
	}
	
	/**
	 * sets the inn's cost per stay
	 * @param neoCost of type int 
	 */
	public void setCost(int neoCost){
		this.cost = neoCost;
	}

}
