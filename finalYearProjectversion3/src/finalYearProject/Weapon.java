package finalYearProject;
/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-03-30         
 */
public class Weapon extends Item{
	private String type; // the generic name of the weapon, for example Axe, sword ect
	private int attackPow; // the base attack power of the weapon

	public Weapon(String deName,int deCost, String deType, int attackpow) {
		super(deName,deCost);
		this.type = deType;
		this.attackPow = attackpow;
	}

	/**
	 * returns the weapon's type   
	 * @return String
	 */
	public String getType() {
		return type;
	}

	/**
	 * sets the weapon's type   
	 * @param neoType of type String
	 */
	public void setType(String neoType) {
		this.type = neoType;
	}

	/**
	 * returns the weapon's attack power   
	 * @return int
	 */
	public int getAttackPow() {
		return attackPow;
	}

	/**
	 * sets the weapon's attack power  
	 * @param NeoAttackPow of type int
	 */
	public void setAttackPow(int NeoAttackPow) {
		attackPow = NeoAttackPow;
	}

}
