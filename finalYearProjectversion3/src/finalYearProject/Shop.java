package finalYearProject;

import java.util.ArrayList;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Shop extends Building {
	private ArrayList<Item>inventory;

	public Shop(int posX, int posY) {
		super(posX, posY);
		inventory = new ArrayList<Item>();
	}
	
	/**
	 * return the i'th item in the shop's inventory ArrayList 
	 *  @param i of type int     
	 * @return Item
	 */
	public Item getithItem(int i){
		return inventory.get(i);
	}
	
	/**
	 * adds an item in the shop's inventory ArrayList      
	 * @param neoItem
	 */
	public void setItem(Item neoItem){
		inventory.add(neoItem);
	}

}
