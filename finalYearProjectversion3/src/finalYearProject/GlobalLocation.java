package finalYearProject;

/**
 * @author      Ruairi Whelan  	   ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class GlobalLocation extends Entity {
	private int posX;
	private int posY;
	private String data; /// holds a quick description
	private String type;
	private String area; // what quarter of the map it belongs to
	
	public GlobalLocation(String entreeName,int neoPosX, int neoPosY, String neoData, String deType, String deArea) {
		super(entreeName);
		this.posX = neoPosX;
		this.posY = neoPosY;
		this.data = neoData;
		this.type = deType;
		this.area = deArea;
	}
	
	/**
	 * sets the X position 
	 * @param neoPosX of type int
	 */ 
	public void setPosX(int neoPosX){
		this.posX = neoPosX;
	}
	
	/**
	 * returns the X position
	 * @return int
	 */ 
	public int getPosX(){
		return posX;
	}
	
	/**
	 * sets the Y position 
	 * @param neoPosY of type int
	 */
	public void setPosY(int neoPosY){
		this.posY = neoPosY;
	}
	
	/**
	 * returns the Y position
	 * @return int
	 */
	public int getPosY(){
		return posY;
	}
	
	/**
	 * sets a detail about the location
	 * @param neoData of type String
	 */
	public void setData(String neoData){
		this.data = neoData;
	}
	
	/**
	 * returns the Y position
	 * @return int
	 */
	public String getData(){
		return data;
	}
	
	/**
	 * set the type of the location
	 * @param neoType of type String
	 */
	public void setType(String neoType){
		this.type = neoType;
	}
	
	/**
	 * returns the type
	 * @return String
	 */
	public String getType(){
		return type;
	}
	
	/**
	 * set the quadrant of the location
	 * @param deQuarter of type String
	 */
	public void setQuarter(String deQuarter){
		this.area = deQuarter; 
	}
	
	/**
	 * returns the quadrant/quarter
	 * @return String
	 */
	public String getQuarter(){
		return area;
	}
	
	/**
	 * returns the toString
	 * @return String
	 */
	public String toString(){
		return super.toString()+ ": it is " + data + " |position: " + posX +"," + posY;
	}

}
