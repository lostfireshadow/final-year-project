package finalYearProject;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Mob extends Creature{
	private String weakness;
	private String strongAgainst;
	private int goldGiven;
	private int expGiven;

	public Mob(String entreeName, int level, int totalHealth,
			int currentHealth, int totalMana,int currentMana, int defence, int attack,
			String weakness,String strongAgainst,int goldGiven,int expGiven) {
		super(entreeName, level, totalHealth, currentHealth, totalMana, currentMana, defence, attack);
		this.weakness = weakness;
		this.strongAgainst = strongAgainst;
		this.goldGiven = goldGiven;
		this.expGiven = expGiven;
	}
	

	/**
	 * return the monster's weakness      
	 * @return an String 
	 */
	public String getWeakness() {
		return weakness;
	}
	
	/**
	 * sets the monster's weakness      
	 * @param weakness of type string 
	 */
	public void setWeakness(String weakness) {
		this.weakness = weakness;
	}

	/**
	 * return what the monster ins strong against     
	 * @return String 
	 */
	public String getStrongAgainst() {
		return strongAgainst;
	}

	/**
	 * sets what the monster is strong against     
	 * @param strongAgainst of type String 
	 */
	public void setStrongAgainst(String strongAgainst) {
		this.strongAgainst = strongAgainst;
	}

	/**
	 * return what the amount of gold awarded for defeating the monster     
	 * @return int
	 */
	public int getGoldGiven() {
		return goldGiven;
	}
	

	/**
	 * sets what the amount of gold awarded for defeating the monster     
	 * @param goldGiven
	 */
	public void setGoldGiven(int goldGiven) {
		this.goldGiven = goldGiven;
	}
	
	/**
	 * return what the amount of experience awarded for defeating the monster     
	 * @return int
	 */
	public int getExpGiven() {
		return expGiven;
	}
	
	/**
	 * sets what the amount of experience awarded for defeating the monster     
	 * @param expGiven of type int
	 */
	public void setExpGiven(int expGiven) {
		this.expGiven = expGiven;
	}

}
