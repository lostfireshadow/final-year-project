package finalYearProject;

import java.util.ArrayList;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Dungeon extends GlobalLocation{
	private int recLevel;
	private ArrayList<String> monsters;
	private ArrayList<Chest> chests;
	public Dungeon(String entreeName, int neoPosX, int neoPosY, String neoData,
			String deType, int deRecLevel, String deArea) {
		super(entreeName, neoPosX, neoPosY, neoData, deType, deArea);
		this.recLevel = deRecLevel;
		monsters = new ArrayList<String>();
		chests = new ArrayList<Chest>();
	}
	
	
	/**
	 * returns the recommended level for the dungeon   
	 * @return int
	 */
	public int getRecLevel(){
		return recLevel;
	}
	
	/**
	 * sets the recommended level for the dungeon   
	 * @param level of type int
	 */
	public void setRecLevel(int level){
		this.recLevel = level;
	}
	
	/**
	 * returns a Chest from an arraylist 
	 * @param i of type int
	 * @return Chest
	 */ 
	public Chest getChestI(int i){
		return chests.get(i);
	}
	
	/**
	 * sets a Sting to the arraylist chests 
	 * @param neoChest of type Chest
	 */
	public void addChest(Chest neoChest){
		chests.add(neoChest);
	}
	
	public  ArrayList<Chest> getChests(){
		return chests;
	}
	
	/**
	 * returns a string  from an arraylist monsters
	 * @param i of type int 
	 * @return int
	 */ 
	public String getMonsterI(int i){
		return monsters.get(i);
	}
	
	/**
	 * sets a string  an arraylist  
	 * @param neoMonster of type String
	 */ 
	public void addMonster(String neoMonster){
		monsters.add(neoMonster);
	}

}
