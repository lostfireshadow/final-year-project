package finalYearProject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-03-30         
 */

public class Main {
	
	
	 private static Texture wood;
	 //private static Texture town;
	 private Texture towntest;
	 private  Texture cave;
	 private  Texture monster;
	 private  Texture background;
	 private  Texture stickmen;
	 private  Texture bigBad;
	 private  Texture player;
	 private  Texture grass;
	 private  Texture itemSymbol;
	
	private ArrayList<TempStatHolding> tempStats;
	
	private static final int WIDTH = 1000;
	private static final int HEIGHT = 600;
	private static final String TITLE = "The Game of Life and Mystery";
	private int Xpos = 50;
	private int Ypos = 50;
	private int internX;
	private int internY;
	private ArrayList <GlobalLocation> ImportantPlaces;
	private ArrayList<Mob> battleMonsterList;
	private boolean isMoving;
	private boolean menuNeed;
	private boolean isShopping;
	private boolean playerTurn;
	private boolean monsterTurn;
	private boolean insideTown;
	private boolean inDungeon;
	private int characterNumber;
	private int totalExp;
	private int totalTempMoney;
	//public ArrayList<Item> inventorytest;
	private Party deParty;
	//public Character testChar;
	private Mob testMob;
	private int monsterOption;
	private int attackedCharacter;
	private int damagetocharacter;  // the amount of damage done to a character in a turn
	private boolean mainGame;
	private boolean characterDefencing;
	private boolean monsterAttack;
	private boolean playerStats;
	private boolean monstersNumbers;
	private boolean selected;
	private boolean inventoryNeeded;
	private boolean itemNeeded;
	private int monNumbers;
	private int currentMonster;
	private int lastMonster;
	private int mouseX;
	private int mouseY;
	private int ResetX; // reset the X position 
	private int resetY;
	private int framesince;
	private String currentArea; 
	private String playerAction;
	private int defendingChar;
	private boolean GameOver;
	private Dungeon CurrentDungeon;
	private boolean isLevelUp;
	private boolean saving;
	private boolean ultimateUnknown;
	private boolean openChest;
	private String chestContained;
	public Main(){
		
		try {
			wood = loadTexture("wood","jpg");
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			cave = loadTexture("caveTemp","png");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			grass= loadTexture("grass","png");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			towntest = loadTexture("towntest","png");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			monster = loadTexture("monster","png");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			background = loadTexture("background","png");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			stickmen = loadTexture("stickman","png");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		try {
			player = loadTexture("overworldSpite","png");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			itemSymbol = loadTexture("item","png");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			bigBad = loadTexture("BigBad","png");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		 
		deParty = new Party();
		ImportantPlaces = new ArrayList <GlobalLocation>();
		battleMonsterList = new ArrayList<Mob>();
		tempStats = new ArrayList<TempStatHolding>();
		isMoving = false;
		menuNeed = false;
		characterDefencing = false;
		isShopping = false;
		mainGame = false;
		monsterAttack = false;
		playerStats = false;
		monstersNumbers = false;
		insideTown = false;
	    inDungeon = false;
		selected = false;
		playerTurn = true;
		monsterTurn = false;
		inventoryNeeded = false;
		itemNeeded = false;
		isLevelUp= false;
		characterNumber = 0;
		damagetocharacter = 0;
		currentArea = "south";
		monNumbers = -1;
		framesince = 0;
		attackedCharacter= -1;
		totalExp = 0;
		totalTempMoney =0;
		monsterOption = -1; // the monsters currenting actions
		currentMonster =0;
		ResetX = 0;
		resetY =0;
		defendingChar = -1;
		playerAction = "null";
		internX = 100;
		internY = 100;
		this.CurrentDungeon = new Dungeon("entreeName", -1, -1,"neoData", "deType", 0, "deArea");
		GameOver = false;
		saving = false;
		ultimateUnknown = false;
		openChest = false;
		chestContained = "NULL";
	}
	
	/**
	 * converts an image in to a useable texture which can printed to the window                          
	 *  the images name and the it file type.         
	 * @return a texture or image.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	
	public  Texture loadTexture(String key, String type) throws FileNotFoundException, IOException{
		//java.net.URL imgURL = getClass().getClassLoader().getResource(".../res/image/"+key +"."+type);
		//return Texture.getTexture( imgURL));
		return TextureLoader.getTexture(type, new FileInputStream(new File( "src/res/image/"+key +"."+type)));
	}
	public void start(){
		run();
	}
	
	public void stop(){
		
	}
	
	public void run(){	
		while(!Window.isCloseRequested()){
			while(Mouse.next()){
				// Mouse inputs, i.e button press on the screen
				mouseX = Mouse.getX();
				mouseY = Mouse.getY();
				if(mainGame == false && Mouse.isButtonDown(0)&& mouseX >= 400 && mouseX <= 500 && mouseY >= 150 && mouseY <= 200 )
				{
					/// this was more difficult that what it should of being 
					mainGame = true;
				}else if (isLevelUp == false&&playerTurn == true && monsterAttack == true &&Mouse.isButtonDown(0)&& mouseX >= 843 && mouseX <=936 && mouseY >= 400 && mouseY <= 450 ){
					//System.out.println("Attack pressed, this is a test \n");
					int temp = 0;
					playerAction ="attack";
					deParty.increaseYanYang();
					deParty.getCharacter(characterNumber).increaseAttackused(1);
					int defence = battleMonsterList.get(temp).getDefence();
					int attack = deParty.getCharacter(characterNumber).getAttack();
					int damage = (attack - defence) + 10; // the 10 is a place holder that will later be calculated based speed and the character's weapon
					defendingChar = characterNumber;
					characterNumber +=1;
					if(characterNumber >= deParty.getParty().size()){
						characterNumber =0;
					}
					//System.out.println("Dealt damage:"+ damage);
					battleMonsterList.get(temp).damageDealt(damage);
					if(battleMonsterList.get(temp).getCurrentHealth() <= 0){
						totalExp += battleMonsterList.get(temp).getExpGiven();
						totalTempMoney += battleMonsterList.get(temp).getGoldGiven(); 
						battleMonsterList.remove(temp);
						this.monNumbers -= 1;
					}
					if(monNumbers <= 0){
						isLevelUp = false;
						playerTurn = true;
						monsterTurn = false;
						playerAction = "null";
						for(int i = 0; i< deParty.getParty().size(); i++){
							Character chartemp = deParty.getCharacter(i);
							if(chartemp.getCurrentHealth() > 0){
								chartemp.addExp(totalExp);
								  if(chartemp.getLevel()> tempStats.get(i).getLevel()){
									  isLevelUp = true;
								  }else{
									  monsterAttack = false;
								  }
							}
						}
						currentMonster = 0;
						characterNumber = 0;
						deParty.addMoney(totalTempMoney);
						totalTempMoney = 0;
						totalExp = 0;
					}
				}else if (monsterAttack == true &&Mouse.isButtonDown(0)&& mouseX >= 843 && mouseX <=936 && mouseY >= 300 && mouseY <= 350 ){
					// modify to set character to defend
					playerAction ="defend";
					deParty.decreaseYanYang();
					deParty.getCharacter(characterNumber).setDefending(true);
					deParty.getCharacter(characterNumber).increaseDefenceused(1);
					characterDefencing = true;
					defendingChar = characterNumber;
					characterNumber +=1;
					if(characterNumber >= deParty.getParty().size()){
						characterNumber = 0;
					}
				}else if (monsterAttack == true &&Mouse.isButtonDown(0)&& mouseX >= 843 && mouseX <=936 && mouseY >= 200 && mouseY <= 250 ){
					//System.out.println("special pressed, this is a test\n");
					Character temp = deParty.getCharacter(characterNumber);
					if(temp.getCurrentMana()>=10){
						    temp.removeMana(10);
							playerAction ="special";
							temp.increaseSpecialused(1);
							int SpecalDamage = temp.getWisdom() + temp.getKnowledge() + (temp.getSpeed()); 
							int j = 0;
							for(int i = 0; i < monNumbers;i++){
								int defence = battleMonsterList.get(i).getWisdom(); 
								int damage = SpecalDamage - defence; 
								if(damage > 0){
									battleMonsterList.get(i).damageDealt(damage);
									j++;
								}
							}
							for(int i = 0; i < battleMonsterList.size();i++){
								if(battleMonsterList.get(i).getCurrentHealth() <= 0){
									totalExp += battleMonsterList.get(i).getExpGiven();
									totalTempMoney += battleMonsterList.get(i).getGoldGiven(); 
									battleMonsterList.remove(i);
									this.monNumbers -= 1;
								}
							}
							defendingChar = characterNumber;
							characterNumber +=1;
							if(characterNumber >= deParty.getParty().size()){
								characterNumber = 0;
								}
						
						if(monNumbers <= 0){
							isLevelUp = false;
							playerTurn = true;
							monsterTurn = false;
							playerAction = "null";
							for(int i = 0; i< deParty.getParty().size(); i++){
								Character chartemp = deParty.getCharacter(i);
								if(chartemp.getCurrentHealth() > 0){
									chartemp.addExp(totalExp);
									 if(chartemp.getLevel()> tempStats.get(i).getLevel()){
										  isLevelUp = true;
									  }else{
										  monsterAttack = false;
									  }
									}
								}
							currentMonster = 0;
							characterNumber = 0;
							deParty.addMoney(totalTempMoney);
							totalTempMoney = 0;
							totalExp = 0;
						}
					}
				}
				else if(isShopping == true && Mouse.isButtonDown(0))
				{
					if(mouseX >= 625 && mouseX <= 650){
						int maxY = 555;
						int minY = 530;
						for(int i = 0; i<deParty.getPI().size();i++){
							//int number = i+0;
							if(mouseY >= minY && mouseY <=maxY){
								Item buy = deParty.getItem(i);
								int deCost = buy.getCost();
								if(deParty.getMoney() >= deCost){
								deParty.removeMoney(deCost);
								buy.setTotal(buy.getTotal()+1);
								}
							}
							maxY -= 30;
							minY-=30;
						}
					}else if(mouseX >= 630 && mouseX <= 681){
						if(mouseY >= 440 && mouseY <=480){
							System.out.println("Ultimate Unknown");
							if(deParty.getMoney() >= 9999999){
								ultimateUnknown = true;
								GameOver = true;
								isShopping = false;
								insideTown = false;
							}
						}
					}else if(mouseX >= 430 && mouseX <= 500 && mouseY >= 150 && mouseY <=200){
						
						if(deParty.getMoney() >= 25){
								for(int i= 0; i<deParty.getParty().size();i++){
									Character tempChar= deParty.getCharacter(i);
									tempChar.setCurrentHealth(tempChar.getTotalHealth());
									tempChar.setCurrentMana(tempChar.getTotalMana());
								}
								deParty.removeMoney(25);
						}
						
					}
				}
				
				else if(mouseX >= 781 && mouseX <=967&& Mouse.isButtonDown(0)){
						if( menuNeed == true){
					    	if(mouseY >= 450 && mouseY <= 500){
					    		if(playerStats == false){
					    			playerStats = true;
					    			inventoryNeeded= false;
					    		}else if(playerStats == true){
					    			playerStats = false;
					    		}
					    	}else if(mouseY >=350 && mouseY <= 400 ){
					    		if(inventoryNeeded == false){
					    			inventoryNeeded = true;
					    			playerStats = false;
					    		}else if(inventoryNeeded == true){
					    			inventoryNeeded= false;
					    		}
					    		//System.out.println(inventoryNeeded);
					    	}else if(mouseY >=250 && mouseY <= 300){
					    		saving = true;
					    		menuNeed = false;
					    		try {
									cleanUp();
								} catch (IOException e) {
									e.printStackTrace();
								}
					    	}
						}
					
				}else if(inventoryNeeded == true &&Mouse.isButtonDown(0)){
	
					if(mouseX >= 50 && mouseX <=100){
						if(mouseY >= 500 && mouseY <= 530){
							MedicalItem temp = (MedicalItem) deParty.getItem(0);
							 int tHealth = temp.getAmount();
							temp.setTotal(temp.getTotal() -1);
							 for (int p = 0; p < deParty.getParty().size(); p++){
								 Character charTemp = deParty.getCharacter(p);
								 if(charTemp .getCurrentHealth() > 0){
									 deParty.getCharacter(p).healDamage(tHealth);
								 }
								
							 }
							}
						else if(mouseY >= 460 && mouseY <= 495){
							Item temp = deParty.getItem(1);
							temp.setTotal(temp.getTotal() -1);	
						}
					}
				}
				//TODO
				/*if ( Mouse.isButtonDown(0)){   // code to double button co-ord to insure that the work
					System.out.println("X position: "+ mouseX+ " Y position:"+ mouseY );
				}*/
			}
			while(Keyboard.next()) {
				/// keybroad commands, mostly is changing Boolean values 
				if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)){
					try {
						cleanUp();
					} catch (IOException e) {
						e.printStackTrace();
					}
		            System.exit(0);
				}
				else if(mainGame == false &&Keyboard.isKeyDown(28) ){
						mainGame = true;
				}else if(insideTown == true && isShopping == true && Keyboard.isKeyDown(28)){
					insideTown = false;
					isShopping = false;
					Xpos= ResetX;
					Ypos=resetY;
					
				}else if (inDungeon == false&&insideTown == false && Keyboard.getEventKey() == Keyboard.KEY_A && isShopping == false && monsterAttack == false && playerStats == false) {
					    Xpos -=10;
					    isMoving = true;
				}else if(inDungeon == false&&insideTown == false && Keyboard.getEventKey() == Keyboard.KEY_D && isShopping == false && monsterAttack == false && playerStats == false) {
					    Xpos +=10; 
					    isMoving = true;
				}else if(inDungeon == false&&insideTown == false && Keyboard.getEventKey() == Keyboard.KEY_W && isShopping == false && monsterAttack == false && playerStats == false) {
					    Ypos +=10; 
					    isMoving = true;
				}else if(inDungeon == false&&insideTown == false && Keyboard.getEventKey() == Keyboard.KEY_S && isShopping == false && monsterAttack == false && playerStats == false) {
					    Ypos -=10; 
					    isMoving = true;
				}else if(Keyboard.isKeyDown(28) && menuNeed== false && monsterAttack == false ) // the return key
				{
						menuNeed =true;
						isShopping = false;
				}else if(Keyboard.isKeyDown(28) && menuNeed== true && monsterAttack == false) // the return key
				{
						menuNeed =false;
						playerStats = false;
						inventoryNeeded = false;
				}else if(inDungeon == true && Keyboard.getEventKey() == Keyboard.KEY_A && isShopping == false && monsterAttack == false && playerStats == false) {
					if(internX > 0){	
					    internX -=10;
					    isMoving = true;
					}
			    }else if(inDungeon == true && Keyboard.getEventKey() == Keyboard.KEY_D && isShopping == false && monsterAttack == false && playerStats == false) {
					if(internX < 600){
			    		internX +=10;
			    		isMoving = true;
					}
			    }else if(inDungeon == true && Keyboard.getEventKey() == Keyboard.KEY_W && isShopping == false && monsterAttack == false && playerStats == false) {
					if(internY < 580){
			    		internY +=10;
			    		isMoving = true;
					}
			    }else if(inDungeon == true && Keyboard.getEventKey() == Keyboard.KEY_S && isShopping == false && monsterAttack == false && playerStats == false) {
					if(internY > 0){
			    		internY -=10;
			    		isMoving = true;
					}
			    }
					
			}
			if(insideTown == false && inDungeon == false){
				if (Xpos >= 600){
					Xpos = 0;
					if(currentArea.equals("west")){
						currentArea = "east";
					}else if (currentArea.equals("east")){
						currentArea = "west";
					}else{
						currentArea = "east";
					}
				}
				else if (Xpos < 0){
					Xpos = 600-10;
					if(currentArea.equals("west")){
						currentArea = "east";
					}else if (currentArea.equals("east")){
						currentArea = "west";
					} else{
						currentArea = "east";
					}
				}
				if(Ypos >= 600){
					Ypos = 0;
					if(currentArea.equals("north")){
						currentArea = "south";
					}else if (currentArea.equals("south")){
						currentArea = "north";
					} else{
						currentArea = "north";
					}
				}
				else if(Ypos <0){
					Ypos = 600 - 50;
					if(currentArea.equals("north")){
						currentArea = "south";
					}else if (currentArea.equals("south")){
						currentArea = "north";
					} else{
						currentArea = "south";
					}
				}
			}else if(inDungeon == true){
				if(internX < 50 && internX > -50 ){
					if( internY > -10 && internY < 50){
					inDungeon = false;
					internX = 100;
					internY = 100;
					Xpos= ResetX;
					Ypos=resetY;
					}
					
				}
				for(int i = 0; i < CurrentDungeon.getChests().size();i++){
					Chest using = CurrentDungeon.getChestI(i);
					int tempX = using.getPosX()+1;
					//System.out.println("internX"+internX +", intenY"+ internY);
					int MaxX = tempX+31;
					int tempY = using.getPosY()-1;
					int MaxY = tempY+31;
					
					if(internX > tempX && internX < MaxX)
						if(internY > tempY && internY < MaxY ){
							if(using.getIsOpened() == false){
								//System.out.println("this is a test");
								using.setIsOpened(true);
								openChest = true;
								String contained = using.getContains();
								int number = using.getNumberOf();
								
								if(contained.equals("gold")){
									deParty.addMoney(number);
								}else{
									for(int j = 0; j < deParty.getPI().size();j++){
										Item tempItem = deParty.getItem(j);
										String itemName = tempItem.getName();
										int input = deParty.getItem(j).getTotal()+number;
										if(itemName.equals(contained)){
											deParty.getItem(j).setTotal(input);
											openChest = true;
										}
									}
								}
								
								chestContained = "chest contained "+ number +" of "+contained +"(s)";
							}
							//TODO
						}
				}
			}
			
			for(int i= 0; i < ImportantPlaces.size();i++){
				if(ImportantPlaces.get(i).getType().equals("Town")){
					int tempX = ImportantPlaces.get(i).getPosX() -30;
					int tmpMaxX = tempX+75;
					int tempY = ImportantPlaces.get(i).getPosY() -30;
					int tmpMaxY = tempY+75;
					if(Ypos > tempY && Ypos < tmpMaxY && Xpos >tempX && Xpos < tmpMaxX ){

						ResetX = ImportantPlaces.get(i).getPosX() -30;
						resetY = ImportantPlaces.get(i).getPosY() -30;
						insideTown = true;
						isShopping = true;
						menuNeed = false;
						//System.out.println("this is a test");
					}
				}else if(ImportantPlaces.get(i).getType().equals("Dungeon")){
					if(inDungeon == false){
						int tempX = ImportantPlaces.get(i).getPosX() -30;
						int tmpMaxX = tempX+75;
						int tempY = ImportantPlaces.get(i).getPosY() -30;
						int tmpMaxY = tempY+75;
						if(Ypos > tempY && Ypos < tmpMaxY && Xpos >tempX && Xpos < tmpMaxX ){
							this.CurrentDungeon = (Dungeon) ImportantPlaces.get(i);
							ResetX = ImportantPlaces.get(i).getPosX() -30;
							resetY = ImportantPlaces.get(i).getPosY() -30;
							inDungeon = true;
							//menuNeed = false;
							//System.out.println("this is a test");
						}
					}
					
				}
			}
			
			render();
		}
	}
	
	/**
	 * renders to the screen                         
	 */
	
	public void render(){
		int killedParty = 0;
		for(int i = 0; i > deParty.getParty().size();i++){
			Character charTemp = deParty.getCharacter(i);
			if(charTemp.getCurrentHealth() <= 0){
				killedParty ++;
			}
		}
		if(killedParty == 4){
			GameOver = true;
		}
		 if (mainGame == false){
				
				Window.renderMainMenuText(background);
			} 
		if(saving == true){
			Window.renderImage(background,0,0,1000);
		}
		else if(GameOver == false && mainGame == true){
			Random rn = new Random();
			if(insideTown== false ||inDungeon == true){
				int encounter = -1;
				if(isMoving){
					encounter = rn.nextInt(25)+1;
				}
				if(encounter == 25){
					monsterAttack = true;
					monstersNumbers = true;
					menuNeed = false;
				}
			}
			setupTexture(800,600);
			if (monsterAttack == true && (insideTown == false||inDungeon ==true) && isLevelUp == false ){
				if(monstersNumbers == true){	
				this.monNumbers = rn.nextInt(5)+1;
					
				for(int i = 0; i < monNumbers;i++){
					int num = i+1;
					Random monsterchooser = new Random();
					int chose = monsterchooser.nextInt(35);
					if(inDungeon == true){
						if(chose == 34){
							String name = "super imp" + num;
							Mob imp = new Mob(name,10,40,40,0,0,35,50,"fire","ice",1500,500);
							battleMonsterList.add(imp);
							System.out.println("testing");
						}else{
							String name = "Captian imp" + num;
							Mob imp = new Mob(name,5,22,17,0,0,45,20,"fire","ice",750,300);
							battleMonsterList.add(imp);
						}
					}else{
						if(chose == 34){
							String name = "Captian imp" + num;
							Mob imp = new Mob(name,5,15,15,0,0,6,20,"fire","ice",500,124);
							battleMonsterList.add(imp);
						}else{
							String name = "imp "+num;
							Mob imp = new Mob(name,1,15,15,0,0,2,10,"fire","ice",100,24);
						    battleMonsterList.add(imp);
						}
					}
						
				//System.out.println("Adding mob");
				}
				monstersNumbers = false;
		      } 
			  Window.renderBattle(battleMonsterList,background, stickmen,bigBad);
			
			}
			else if(insideTown == false && inDungeon == true && isLevelUp == false){
				Window.renderDungeon(grass,600,800,internX,internY,player,CurrentDungeon,background);
				//Window.backgroundScreen(background);
			} else if(isShopping == false &&  monsterAttack == false && mainGame == true && insideTown == false){
				Window.render(Xpos,Ypos, ImportantPlaces, currentArea,towntest, cave,player, background);
			}else if (isShopping == true && mainGame == true) {
				Window.renderShop(background,towntest);
				int tempXpos = 500;
				int tempYpos = 530;
				for (int i = 0; i < deParty.getPI().size();i++){
					Window.renderImage(itemSymbol, tempXpos,tempYpos,20);
					tempYpos -= 30;
				}
				tempXpos += 5;
				tempYpos -= 30;
				Window.renderImage(itemSymbol, tempXpos,tempYpos,40);
				
			}else if(isLevelUp == true){
				Window.backgroundScreen(background);
			}
			 if (menuNeed == true){
					Window.renderMenu( menuNeed, playerStats, inventoryNeeded, itemSymbol,background);
					if(inventoryNeeded == true){
						   renderInventory();
					}
				}
		} else if(GameOver == true){
			if(ultimateUnknown == true){
				Window.renderImage(background,0,0,1000);
			}
		}
	    setupText(800,600);
	    renderText();
	    isMoving = false;
	    Display.update();
		Display.sync(100);
			
		    
	}
	
	/**
	 *renders text to the screen 
	 */
	public void renderText(){
		//Window.rendertext(testChar.toString(),100, 50);
		if(saving == true){
			 if(framesince <= 75){
					Window.rendertextbattle("the game is saving wait waited a moment", 75,50 );
					framesince++;
				}else if(framesince >= 75){
					saving = false;
				}
		} 
		if(insideTown == false && inDungeon == true && monsterAttack == false){
		   if(openChest == true) {
				 if(framesince <= 150){
						Window.rendertextbattle(chestContained, 75,50 );
						framesince++;
					}else if(framesince >= 150){
						openChest = false;
						framesince = 0;
					} 
			}else if(openChest == false && isLevelUp == false && inventoryNeeded==false && playerStats== false && saving == false){
				Window.rendertextbattle("Dungeon",75, 75);
			}
		}else if(ultimateUnknown == true){
			Window.rendertextbattle("after many long quests and grueling battle the party aquired the money", 75,50 );
			Window.rendertextbattle("to buy, the medallion that was said to hold untold powers ", 75,75 );
			Window.rendertextbattle("who knows what power lies hidden in the tinket... ", 75,100 );
			Window.rendertextbattle("or who might try and take it for our heroes", 75,125 );
		}
		if(menuNeed == true){
			
			Window.rendertext("menu",700, 50);
			Window.rendertext("party",680, 115);
			Window.rendertext("inventory",680, 200);
			Window.rendertext("save",680, 300);
			if(playerStats == true){
				int posx = 100;
				int posy = 50; 
				
				for(int i = 0; i< deParty.getParty().size(); i++ ){
					//System.out.println("the size of the party is:" + deParty.getParty().size());
					Character Details = deParty.getCharacter(i);
					String namelv = "name: " +Details.getName()+ " level: " + Details.getLevel();
					String health = "health: "+ Details.getCurrentHealth()+"/"+ Details.getTotalHealth();
					String AttDef = "Str: "+ Details.getAttack() + " Def: "+ Details.getDefence() +" Spd: "+ Details.getSpeed();
					String magicStats = "Wis: "+ Details.getWisdom() + " know: "+ Details.getKnowledge();
					String mana = "mana: "+ Details.getCurrentMana()+"/"+ Details.getTotalMana();
					String exp = "exp gained: " + deParty.getCharacter(i).getExperience();
					posx = 100;
					Window.rendertext(namelv,posx, posy);
					posy = posy+25;
					Window.rendertext(health,posx, posy);
					posx+= 125;
					Window.rendertext(AttDef,posx, posy);
					posx-= 125;
					//posy = 100;
					posy = posy+25;
					Window.rendertext(mana,posx, posy);
					posx+= 125;
					Window.rendertext(magicStats,posx, posy);
					posx-= 125;
					posy = posy+25;
					Window.rendertext(exp,posx, posy);
					posy +=50;
				}
			}else if(inventoryNeeded == true){
				int posx = 100;
				int posy = 75; 
				
				for(int i = 0; i< deParty.getPI().size(); i++ ){
					String ItemDetails = ""+deParty.getItem(i).getName()+": "+ deParty.getItem(i).getTotal();
					Window.rendertext(ItemDetails,posx, posy);
					posy +=30;
				}
				String gold = "Gold:"+ deParty.getMoney();
				Window.rendertext(gold,400,500);
			}
		}else if(isShopping == true && monsterAttack == false && ultimateUnknown == false){
			int x = 50;
			int ypos = 50;
			int x2 = 400;
			for (int i = 0; i <deParty.getPI().size();i++){
				String itemname = deParty.getItem(i).getName()+ ":\t "+ deParty.getItem(i).getTotal()+":\t costs:"+deParty.getItem(i).getCost() ;
				Window.rendertext(itemname,x, ypos);
				Window.rendertext("buy more", x2, ypos);
				ypos= ypos+30;
			}
			ypos=ypos+25;
			Window.rendertext("Ultimate Unknown:\t 0 \t cost: 9,999,999 gold",x, ypos);
			Window.rendertext("buy it", x2, ypos);
			
			String gold = "Gold:"+ deParty.getMoney();
			Window.rendertext(gold,600,500);
			Window.rendertext("rest at an inn",400,420);
			
		}
		else if(monsterAttack == true && isLevelUp == false){
			Window.rendertextbattle("Attack",690, 170);
			Window.rendertextbattle("Defend",690, 270);
			Window.rendertextbattle("Special",690, 370);
			int x = 50;
			int y = 350;
			for(int i=0; i < deParty.getParty().size();i++){
				String name = deParty.getCharacter(i).getName();
				String hitPoint = "HP: "+deParty.getCharacter(i).getCurrentHealth()+"/"+deParty.getCharacter(i).getTotalHealth() ;
				String Mana = "MP: " +deParty.getCharacter(i).getCurrentMana()+"/"+ deParty.getCharacter(i).getTotalMana();
				Window.rendertextbattle(name,x, y);
				y+= 25;
				Window.rendertextbattle(hitPoint,x, y);
				y+= 25;
				Window.rendertextbattle(Mana,x, y);
				y-=50;
				x +=100;
			}
			if(playerTurn == true){
				if(playerAction.equals("null")){
					Window.rendertextbattle(deParty.getCharacter(characterNumber).getName() +" turn", 75,50 );
				}
				 if(playerAction.equals("attack")){
					 if(framesince <= 100){
							Window.rendertextbattle(deParty.getCharacter(defendingChar).getName() +" lunched an attack", 75,50 );
							framesince++;
						}else if(framesince >= 100){
							playerTurn = false;
							monsterTurn = true;
							playerAction = "null";
							selected = false;
							framesince = 0;
							
						}
				 } else if(playerAction.equals("defend")){
					 if(framesince <= 100){
							Window.rendertextbattle(deParty.getCharacter(defendingChar).getName() +" defended", 75,50 );
							framesince++;
						}else if(framesince >= 100){
							playerTurn = false;
							monsterTurn = true;
							playerAction = "null";
							selected = false;
							framesince = 0;
						}
				 }else if(playerAction.equals("special")){
					 if(framesince <= 100){
							Window.rendertextbattle(deParty.getCharacter(defendingChar).getName() +" used a special attack", 75,50 );
							framesince++;
						}else if(framesince >= 100){
							playerTurn = false;
							monsterTurn = true;
							selected = false;
							playerAction = "null";
							framesince = 0;
						}
				 }
				
			}else if(playerTurn == false){
				if(currentMonster >= battleMonsterList.size())
				{
					currentMonster = 0;
				}
				if(monsterTurn == true){
					if(framesince <= 100){
						Window.rendertextbattle(battleMonsterList.get(currentMonster).getName() +"'s turn", 75,50 );
						framesince++;
					}else if(framesince >= 100){
						monsterTurn = false;
						framesince = 0;
					}
					
				}else if(monsterTurn == false){
	                Random rn2 = new Random();
					Random rn3 = new Random();
					
					if(currentMonster >= battleMonsterList.size())
					{
						currentMonster = 0;
					}
					if(selected == false && battleMonsterList.size() > 0)
					 {
						
					    //System.out.println(attackedCharacter);
					    monsterOption = rn3.nextInt(3);
					    //System.out.println(monsterOption);
						attackedCharacter= rn2.nextInt( deParty.getParty().size());
						//System.out.println("testing");
						int yinYang = deParty.getYinYang();
						if (monsterOption == 0 && yinYang == 0 ||monsterOption == 1 && yinYang > 0 ){
							int defence = deParty.getCharacter(attackedCharacter).getDefence();
							int attack = battleMonsterList.get(currentMonster).getAttack();
							damagetocharacter = (attack - defence);
							if(damagetocharacter < 0){
								damagetocharacter = 0;
							}
							characterDefencing = false;
							deParty.getCharacter(attackedCharacter).damageDealt(damagetocharacter);
						}else if(monsterOption  == 1 && yinYang > 0 ){
							int defence = deParty.getCharacter(attackedCharacter).getDefence();
							int attack = battleMonsterList.get(currentMonster).getAttack();
							damagetocharacter = (attack - defence);
							if(damagetocharacter < 0){
								damagetocharacter = 0;
							}else{
								damagetocharacter*=2;
							}
							characterDefencing = false;
							deParty.getCharacter(attackedCharacter).damageDealt(damagetocharacter);
						}else if(monsterOption  == 1 && yinYang < 0){
							int defence = deParty.getCharacter(attackedCharacter).getDefence();
							int attack = battleMonsterList.get(currentMonster).getAttack();
							damagetocharacter = (attack - defence);
							if(damagetocharacter < 0){
								damagetocharacter = 0;
							}else{
								damagetocharacter/=2;
							}
							characterDefencing = false;
							deParty.getCharacter(attackedCharacter).damageDealt(damagetocharacter);
							
						}
						//deParty.getCharacter(attackedCharacter).damageDealt(1);
						selected = true;
						//System.out.println(attackedCharacter);
					} // add else statement, maybe
					if(deParty.getYinYang() == 0){
						//System.out.println("relexed");
						if (monsterOption == 0){
							 if(monNumbers > 0){
									if(selected == true&& framesince <= 100){
										Window.rendertextbattle("party member "+ deParty.getCharacter(attackedCharacter).getName()+ " was attacked,"+damagetocharacter+ "done", 75,50 );
										
										framesince++;
									}else if(selected == true&&framesince >= 100){
										playerTurn = true;
										monsterTurn = false;
										framesince = 0;
										selected = false;
										damagetocharacter = 0;
										currentMonster++;
									}
						 }
						}else if(monsterOption == 1){
							if(framesince <= 100){
								Window.rendertextbattle("party member "+ deParty.getCharacter(attackedCharacter).getName()+ " was attacked,"+damagetocharacter+ "done", 75,50 );
								framesince++;
							}else if(framesince >= 100){
								playerTurn = true;
								monsterTurn = false;
								framesince = 0;
								selected = false;
								currentMonster++;
							}
						}else{
							if(framesince <= 100){
								Window.rendertextbattle(battleMonsterList.get(currentMonster).getName() +" stared blankly at the sun", 75,50 );
								framesince++;
							}else if(framesince >= 100){
								playerTurn = true;
								monsterTurn = false;
								framesince = 0;
								selected = false;
								currentMonster++;
							}
						}	
						
					}else if(deParty.getYinYang() > 0){
						//System.out.println("Raging");
							if (monsterOption == 0){
								 if(monNumbers > 0){
										if(selected == true&& framesince <= 100){
											Window.rendertextbattle("party member "+ deParty.getCharacter(attackedCharacter).getName()+ " was attacked,"+damagetocharacter+ "done", 75,50 );
											
											framesince++;
										}else if(selected == true&&framesince >= 100){
											playerTurn = true;
											framesince = 0;
											selected = false;
											damagetocharacter = 0;
											currentMonster++;
										}
								 	}
								}else if(monsterOption == 1){
									if(framesince <= 100){
										Window.rendertextbattle("party member "+ deParty.getCharacter(attackedCharacter).getName()+ " was attacked,"+damagetocharacter+ "done", 75,50 );
										framesince++;
									}else if(framesince >= 100){
										playerTurn = true;
										monsterTurn = false;
										framesince = 0;
										selected = false;
										currentMonster++;
									}
								}else{
									if(framesince <= 100){
										Window.rendertextbattle(battleMonsterList.get(currentMonster).getName() +" stared blankly at the sun", 75,50 );
										framesince++;
									}else if(framesince >= 100){
										playerTurn = true;
										monsterTurn = false;
										framesince = 0;
										selected = false;
										currentMonster++;
									}
								}	
								
							}else if(deParty.getYinYang() < 0){
								//System.out.println("calm");
							if (monsterOption == 0){
								 if(monNumbers > 0){
										if(selected == true&& framesince <= 50){
											Window.rendertextbattle("party member "+ deParty.getCharacter(attackedCharacter).getName()+ " was attacked,"+damagetocharacter+ "done", 75,50 );
											
											framesince++;
										}else if(selected == true&&framesince >= 50){
											playerTurn = true;
											monsterTurn = false;
											framesince = 0;
											selected = false;
											damagetocharacter = 0;
											currentMonster++;
										}
							        }
							    }else if(monsterOption == 1){
									if(framesince <= 100){
										Window.rendertextbattle("party member "+ deParty.getCharacter(attackedCharacter).getName()+ " was attacked,"+damagetocharacter+ "done", 75,50 );
										framesince++;
									}else if(framesince >= 100){
										playerTurn = true;
										framesince = 0;
										selected = false;
										currentMonster++;
										}
									}else{
									if(framesince <= 100){
										Window.rendertextbattle(battleMonsterList.get(currentMonster).getName() +" stared blankly at the sun", 75,50 );
										framesince++;
									}else if(framesince >= 100){
										playerTurn = true;
										monsterTurn = false;
										framesince = 0;
										currentMonster++;
										}
									}	
							
							} 
				}
				
				
		} 
	}
		
		else if(mainGame == false){
			Window.rendertext("Hit the enter key to continue,",300, 300);
			
		}else if( isLevelUp == true){
			
			if(framesince <= 500){
				int posx = 100;
				int posy = 50; 
				
				for(int i = 0; i< deParty.getParty().size(); i++ ){
					//System.out.println("the size of the party is:" + deParty.getParty().size());
					Character Details = deParty.getCharacter(i);
					TempStatHolding holding = tempStats.get(i);
					String namelv = "name: " +Details.getName()+ " level: " + Details.getLevel();
					
					int attackDif = Details.getAttack() - holding.getAttack();
					//System.out.println("Attack " +attackDif);
					int defDif = Details.getDefence() - holding.getDefence();
					int spdDif = Details.getSpeed() - holding.getSpeed();
					String AttDef = "Str: +"+ attackDif + " Def: +"+ defDif +" Spd: +"+ spdDif;
					
					int wisdomdif = Details.getWisdom() - holding.getWisdom();
					int knowdif = Details.getKnowledge() - holding.getKnowledge();
					String magicStats = "Wis: +"+ wisdomdif + " know: +"+ knowdif;
					String mana = "mana: "+ Details.getCurrentMana()+"/"+ Details.getTotalMana();
					posx = 100;
					Window.rendertext(namelv,posx, posy);
					posx+= 200;
					Window.rendertext(AttDef,posx, posy);
					posx-= 125;
					//posy = 100;
					posy = posy+25;
					Window.rendertext(mana,posx, posy);
					posx+= 125;
					Window.rendertext(magicStats,posx, posy);
					posx-= 125;
					posy +=50;
				}
				framesince++;
			}else if(framesince >= 500){
				playerTurn = true;
				monsterAttack = false;
				isLevelUp = false;
				framesince = 0;
				characterNumber = 0;
				for(int i = 0; i< tempStats.size(); i++ ){
					TempStatHolding num = tempStats.get(i);
					Character tempChar = deParty.getCharacter(i);
					num.setLevel(tempChar.getLevel());
					num.setDefence(tempChar.getDefence());
					num.setAttack(tempChar.getAttack());
					num.setSpeed(tempChar.getSpeed());
					num.setWisdom(tempChar.getWisdom());
					num.setKnowledge(tempChar.getKnowledge());
					
				}
			}
				
			if (currentMonster > battleMonsterList.size()){
				currentMonster = 0;
			}
		}else if(GameOver == false){
			Window.rendertext(currentArea,50, 25);
		}
		
		
		Character check = deParty.getCharacter(characterNumber);
		if(check.getCurrentHealth() <= 0){
			characterNumber++;
			if(characterNumber > deParty.getParty().size()){
				characterNumber = 0;
			}
		}
}
		
	
	/**
	 * sets up the screen in order to print textures                         
	 *  the height and width of the window in use .         
	 */
	public void setupTexture(int width, int height){
    	GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glShadeModel(GL11.GL_SMOOTH);       
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDisable(GL11.GL_LIGHTING);                   

        GL11.glClearColor(0.5f,0.5f,1.0f, 0.0f);               
        GL11.glClearDepth(1);                                      

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        //GL11.glViewport(0,0,width,height);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        //GL11.glOrtho(0, width, height, 0, 1, -1);
        GL11.glOrtho(0, 800, 0, 600, 1, -1);
        //
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
    }
	
	public void renderInventory(){
		/// will looped to a window.render method that will render an a image for each item in
		// the inventory
		int xPos = 50;
		int yPos = 500;
		for(int i = 0; i< deParty.getPI().size();i++){
			Window.renderImage(itemSymbol, xPos,yPos, 30);
			yPos -= 35;
		}
	}
	
	/**
	 * sets up the screen in order to print text to the screen                       
	 *  the height and width of the window in use         
	 */
	public void setupText(int width, int height){
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	    GL11.glShadeModel(GL11.GL_SMOOTH);       
	    GL11.glDisable(GL11.GL_DEPTH_TEST);
	    GL11.glDisable(GL11.GL_LIGHTING);                   

	    GL11.glClearColor(0.5f,0.5f,1.0f, 0.0f);               
	    GL11.glClearDepth(1);                                      

	    GL11.glEnable(GL11.GL_BLEND);
	    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

	    //GL11.glViewport(0,0,width,height);
	    GL11.glMatrixMode(GL11.GL_MODELVIEW);

	    GL11.glMatrixMode(GL11.GL_PROJECTION);
	    GL11.glLoadIdentity();
	    GL11.glOrtho(0, width, height, 0, 1, -1);
	    //GL11.glOrtho(0, 800, 0, 600, 1, -1);
	    //
	    GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}

	
	public void cleanUp() throws IOException {
		File file = new File("./GameSave.txt");
		if (!file.exists()) {
				file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		String xpos = Xpos+"";
		String ypos = Ypos+"";
		bw.write(xpos);
		bw.newLine();
		bw.write(ypos);
		bw.newLine();
		bw.write(currentArea);
		bw.newLine();
		String money = deParty.getMoney()+"";
		bw.write(money);
		bw.newLine();
		for(int i = 0; i < deParty.getParty().size();i++){
			Character temp = deParty.getCharacter(i);
			String level = temp.getLevel()+"";
			String TotalHealth = temp.getTotalHealth()+"";
			String CurrentHealth = temp.getCurrentHealth()+"";
			String totalMana = temp.getTotalMana()+"";
			String currentMana = temp.getCurrentMana()+"";
			String attack = temp.getAttack()+"";
			String defence = temp.getDefence()+"";
			String wisdom = temp.getWisdom()+"";
			String knowledge = temp.getKnowledge()+"";
			String speed = temp.getSpeed()+"";
			String exp = temp.getExperience()+"";
			bw.write(level);
			bw.newLine();
			bw.write(TotalHealth);
			bw.newLine();
			bw.write(CurrentHealth);
			bw.newLine();
			bw.write(totalMana);
			bw.newLine();
			bw.write(currentMana);
			bw.newLine();
			bw.write(attack);
			bw.newLine();
			bw.write(defence);
			bw.newLine();
			bw.write(wisdom);
			bw.newLine();
			bw.write(knowledge);
			bw.newLine();
			bw.write(speed);
			bw.newLine();
			bw.write(exp);
			bw.newLine();
		}
		for(int i = 0; i < deParty.getPI().size();i++){
			String itemTotal = deParty.getItem(i).getTotal() +"";
			bw.write(itemTotal);
			bw.newLine();
		}
		bw.close();
		
	}
	public static void main(String[] args) throws IOException {
		
        Window.createWindow(WIDTH, HEIGHT, TITLE);
		Main game = new Main();
		game.settingImportantPlaces();
		game.settingInventory();
		game.settingupParty();
		game.upDateParty();
		game.start();

	}
	
	private void upDateParty() throws IOException{
		File file = new File("./GameSave.txt");
		if (!file.exists()) {
				file.createNewFile();
		}else{
			FileReader reader = new FileReader(file.getAbsoluteFile());
			BufferedReader Bufread= new BufferedReader(reader);
			int loop = 0;
			String line;
			while ((line = Bufread.readLine()) != null) {
				
		        if(loop == 0){
		        	int test = Integer.parseInt(line);
		        	this.Xpos = test;
		        }else if(loop == 1){
		        	int test = Integer.parseInt(line);
		        	this.Ypos = test;
		        }else if(loop == 2){
		        	this.currentArea = line;
		        }else if(loop == 3){
		        	int test = Integer.parseInt(line);
		        	deParty.setmoney(test);
		        }else if(loop == 4){
		        	///Character Number one
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setLevel(test);
		        }else if(loop == 5){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setTotalHealth(test);
		        }else if(loop == 6){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setCurrentHealth(test);
		        }else if(loop == 7){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setTotalMana(test);
		        }else if(loop == 8){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setCurrentMana(test);
		        }else if(loop == 9){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setAttack(test);
		        }else if(loop == 10){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setDefence(test);
		        }else if(loop == 11){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setWisdom(test);
		        }else if(loop == 12){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setKnowledge(test);
		        }else if(loop == 13){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setSpeed(test);
		        }else if(loop == 14){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(0).setExp(test);
		        	deParty.getCharacter(0).setNextLevel(test);
		        }else if(loop == 15){
		        	///Character Number two
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setLevel(test);
		        }else if(loop == 16){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setTotalHealth(test);
		        }else if(loop == 17){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setCurrentHealth(test);
		        }else if(loop == 18){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setTotalMana(test);
		        }else if(loop == 19){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setCurrentMana(test);
		        }else if(loop == 20){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setAttack(test);
		        }else if(loop == 21){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setDefence(test);
		        }else if(loop == 22){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setWisdom(test);
		        }else if(loop == 23){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setKnowledge(test);
		        }else if(loop == 24){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setSpeed(test);
		        }else if(loop == 25){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(1).setExp(test);
		        	deParty.getCharacter(1).setNextLevel(test);
		        }else if(loop == 26){
		        	///Character Number three
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setLevel(test);
		        }else if(loop == 27){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setTotalHealth(test);
		        }else if(loop == 28){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setCurrentHealth(test);
		        }else if(loop == 29){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setTotalMana(test);
		        }else if(loop == 30){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setCurrentMana(test);
		        }else if(loop == 31){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setAttack(test);
		        }else if(loop == 32){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setDefence(test);
		        }else if(loop == 33){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setWisdom(test);
		        }else if(loop == 34){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setKnowledge(test);
		        }else if(loop == 35){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setSpeed(test);
		        }else if(loop == 36){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(2).setExp(test);
		        	deParty.getCharacter(2).setNextLevel(test);
		        }else if(loop == 37){
		        	///Character Number four
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setLevel(test);
		        }else if(loop == 38){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setTotalHealth(test);
		        }else if(loop == 39){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setCurrentHealth(test);
		        }else if(loop == 40){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setTotalMana(test);
		        }else if(loop == 41){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setCurrentMana(test);
		        }else if(loop == 42){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setAttack(test);
		        }else if(loop == 43){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setDefence(test);
		        }else if(loop == 44){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setWisdom(test);
		        }else if(loop == 45){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setKnowledge(test);
		        }else if(loop == 46){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setSpeed(test);
		        }else if(loop == 47){
		        	int test = Integer.parseInt(line);
		        	deParty.getCharacter(3).setExp(test);
		        	deParty.getCharacter(3).setNextLevel(test);
		        }else if(loop == 48){
		        	int test = Integer.parseInt(line);
		        	deParty.getItem(0).setTotal(test);
		        }else if(loop == 49){
		        	int test = Integer.parseInt(line);
		        	deParty.getItem(1).setTotal(test);
		        }else if(loop == 50){
		        	int test = Integer.parseInt(line);
		        	deParty.getItem(2).setTotal(test);
		        }else if(loop == 51){
		        	int test = Integer.parseInt(line);
		        	deParty.getItem(3).setTotal(test);
		        }
		        loop++;
		    }
			Bufread.close();
		}
	}
	
	/**
	 * sets up the party for the game                           
	 */
	private void settingupParty(){
		Character testChar = new Character("Finn", 1, 25,25, 10, 10, 5,5);
		Character testChar2 = new Character("George", 1, 25,25, 10, 10, 5,5);
		Character testChar3 = new Character("Joan", 1, 25,25, 10, 10, 5,5);
		Character testChar4 = new Character("Tempest", 1, 25,25, 10, 10, 5,5);
		deParty.newCharacter(testChar);
		deParty.newCharacter(testChar2);
		deParty.newCharacter(testChar3);
		deParty.newCharacter(testChar4);
		
		for(int i = 0; i < deParty.getParty().size(); i++){
			Character num = deParty.getCharacter(i);
			int level = num.getLevel();
			int defence = num.getDefence();
			int Attack = num.getAttack();
			int speed = num.getSpeed();
			int wisdom = num.getWisdom();
			int knowlegde = num.getKnowledge();
			TempStatHolding temp = new TempStatHolding(level,defence,Attack,speed,wisdom,knowlegde);
			tempStats.add(temp);
		}
			
	}
	
	private void settingInventory() {
		MedicalItem test = new MedicalItem("Heal Bar",50, true, 25);
		test.setTotal(7);
		MedicalItem test2 = new MedicalItem("Mana Bar",75, false, 25);
		test2.setTotal(3);
		deParty.newItem(test);
		deParty.newItem(test2);
		
	}
	
	/**
	 * sets up a list of places in the game                         
	 */
	public void settingImportantPlaces(){
		Town TestTown = new Town("the first town", 150, 150, "a large village in the middle of nowhere", "Town", "south");
		Dungeon testDungeon = new Dungeon("", 400, 400, "a test that should fail", "Dungeon",5,"south");
		Chest testChest1 = new Chest(450,350,"Heal Bar", 5);
		Chest testChest2 = new Chest(100,200,"gold", 1000);
		testDungeon.addChest(testChest1);
		testDungeon.addChest(testChest2);
		ImportantPlaces.add(TestTown);
		ImportantPlaces.add(testDungeon);
		Town TestTownNor = new Town("the second town", 500, 350, "a large village in the middle of nowhere", "Town", "north");
		ImportantPlaces.add(TestTownNor);
		Town TestTownWest = new Town("the second town", 500, 350, "a large village in the middle of nowhere", "Town", "west");
		ImportantPlaces.add(TestTownWest);
	}
	
	/**
	 * sets up an Arraylist that contains positions it used for testing                       
	 * @return an arrylist that contains positions          
	 */
	public ArrayList<GlobalLocation> settingUpArraylist(){
		ArrayList<GlobalLocation> locations = new ArrayList<GlobalLocation>();
		for (int i = 1;i <= 2; i++)
		{
			for (int j = 1;j <= 5; j++){
				GlobalLocation newLocation = new GlobalLocation("field", j, i, "an empty field", "null","South");
				locations.add(newLocation);
			}
		}
		
		int monster = 3;
		int tresure = 5;
		int people = 8;
		locations.get(monster).setData("it a monster");
		locations.get(monster).setType("monster");
		
		locations.get(tresure).setData("it a chest");
		locations.get(tresure).setType("chest");
		locations.get(people).setData("it a person");
		locations.get(people).setType("person");
		return locations;
		
		
	}
}
