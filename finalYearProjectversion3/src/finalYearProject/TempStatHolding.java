package finalYearProject;
/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-03-30          
 */
public class TempStatHolding {
	private int level;
	private int defence;
	private int attack;
	private int speed;
	private int wisdom;
	private int knowledge;
	
	public TempStatHolding(int level,int defence, int attack, int speed,int wisdom, int knowledge){
		this.level = level;
		this.defence = defence;
		this.attack = attack;
		this.speed = speed;
		this.wisdom= wisdom;
		this.knowledge = knowledge;
	}


	/**
	 * returns the temp level     
	 * @return int
	 */
	public int getLevel() {
		return level;
	}
     
	/**
	 * sets the temp level   
	 * @param level of type int
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * return the temp defence   
	 * @return int
	 */
	public int getDefence() {
		return defence;
	}

	/**
	 * sets the creature's defence   
	 * @param defence of type int
	 */
	public void setDefence(int defence) {
		this.defence = defence;
	}
	
	/**
	 * return the creature's attack   
	 * @return int
	 */
	public int getAttack() {
		return attack;
	}

	/**
	 * sets the creature's attack   
	 * @param attack of type int
	 */
	public void setAttack(int attack) {
		this.attack = attack;
	}

	/**
	 * returns the temp speed     
	 * @return int
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * sets the temp speed   
	 * @param speed of type int
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	/**
	 * returns the temp wisdom     
	 * @return int
	 */
	public int getWisdom() {
		return wisdom;
	}

	/**
	 * sets the temp wisdom   
	 * @param wisdom of type int
	 */
	public void setWisdom(int wisdom) {
		this.wisdom = wisdom;
	}

	/**
	 * returns the temp knowledge    
	 * @return int
	 */
	public int getKnowledge() {
		return knowledge;
	}

	/**
	 * sets the temp knowledge   
	 * @param knowledge of type int
	 */
	public void setKnowledge(int knowledge) {
		this.knowledge = knowledge;
	}

}
