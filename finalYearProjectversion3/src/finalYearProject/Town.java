package finalYearProject;

import java.util.ArrayList;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */


public class Town extends GlobalLocation{
	private ArrayList<Building> buildings;
	private ArrayList<Person> people;

	public Town(String entreeName, int neoPosX, int neoPosY, String neoData,
			String deType, String deArea) {
		super(entreeName, neoPosX, neoPosY, neoData, deType, deArea);
		buildings = new ArrayList<Building>();
		people = new ArrayList<Person>();
	}
	
	/**
	 * add a person to the town   
	 * @param neoPerson of type Person
	 */
	public void addPerson(Person neoPerson){
		people.add(neoPerson);
	}
	
	/**
	 * return with the i'th person to the town
	 * @param i    
	 * @return Person
	 */
	public Person getPersonIth(int i){
		return people.get(i);
	}
	
	/**
	 * add a person to the town   
	 * @param neoBuilding of type Building
	 */
	public void addBuilding(Building neoBuilding){
		buildings.add(neoBuilding);
	}
	
	/**
	 * get the i'th person in the town
	 * @param i    
	 * @return Building
	 */
	public Building getBuildingIth(int i){
		return buildings.get(i);
	}

}
