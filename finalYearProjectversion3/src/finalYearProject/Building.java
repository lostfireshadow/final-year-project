package finalYearProject;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */
public class Building {
	private int posX;
	private int posY;
	
	public Building(int posX, int posY){
		this.posX = posX;
		this.posY = posY;
	}
		
	/**
	 * return the building's x position       
	 * @return an integer 
	 */
	public int getPosX() {
		return posX;
	}
	
	/**
	 * sets the x position of the building        
	 * @param posX of type int
	 */
	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	/**
	 * return the building's Y position       
	 * @return an integer 
	 */
	public int getPosY() {
		return posY;
	}
	/**
	 * sets the x position of the building        
	 * @param posY of type int
	 */
	public void setPosY(int posY) {
		this.posY = posY;
	}

}
