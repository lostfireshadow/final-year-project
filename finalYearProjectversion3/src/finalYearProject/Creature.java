package finalYearProject;


/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Creature extends Entity{
	private int level;
	private int totalHealth;
	private int currentHealth;
	private int totalMana;
	private int currentMana;
	private int defence;
	private int attack;
	private int speed;
	private int wisdom;
	private int knowledge;
	private int DefendingUp; // character's defence stat while defending
	private boolean defending;
	
	private int attackused;
	private int defenceused;
	private int specialused;
	

	public Creature(String entreeName,int level,int totalHealth,int currentHealth,int totalMana, int currentMana, int defence,int attack) {
		super(entreeName);
		this.level = level;
		this.totalHealth = totalHealth;
		this.currentHealth = currentHealth;
		this.totalMana = totalMana;
		this.currentMana = currentMana;
		this.defence = defence;
		this.attack = attack;
		this.speed = 2;
		this.wisdom= 4;
		this.knowledge = 5;
		
		this.defending = false;
		this.attackused = 0;
		this.defenceused = 0;
		this.specialused = 0;
	}


	/**
	 * returns the level of the selected creature      
	 * @return int
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * sets level of the creature   
	 * @param level of type int
	 */
	public void setLevel(int level) {
		this.level = level;
	}
	
	/**
	 * a test method used to generate levels    
	 * @param totalexp of type int
	 */
	public int levelgenetor(int totalexp){
		int basic = 50;
		int temp = totalexp;
		int levels = 1;
		while(basic < temp){
			levelup();
			levels++;
			int dif = basic/2;
			basic+=dif;
			
		}
		return levels;
	}
	
	/**
	 * this method increase all stats by a hard coded amount and increase the level by one
	 */
	public void levelup(){
		int curLv = getLevel();
		this.level = curLv+1;
		this.totalHealth += 5;
		this.totalMana += 5;
		this.currentHealth +=5;
		this.currentMana +=5;
		this.attack+= 3;
		this.defence += 2;
		this.wisdom+=2;
		this.knowledge += 2;
		this.speed +=2;
	}
	
	/**
	 * this method increase stats based on the character actions during the game
	 */
	public void LevelUpAffected(){
		if(getLevel() <= 99){
			int curLv = getLevel();
			this.level = curLv+1;
			this.totalHealth += 5;
			this.totalMana += 5;
			this.currentHealth +=5;
			this.currentMana +=5;
			if(attackused ==defenceused && defenceused ==specialused ){
				
				this.attack+= 3;
				this.defence += 2;
				this.wisdom+=2;
				this.knowledge += 2;
				this.speed +=2;
			}else if(attackused >=defenceused && attackused >=specialused){
				this.attack+= 5;
				if(defenceused ==specialused ){
					this.defence += 2;
					this.wisdom+=2;
					this.knowledge += 2;
					this.speed +=2;
				}else if (defenceused >specialused){
					this.defence += 4;
					this.speed +=4;
				}else if(defenceused <specialused){
					this.wisdom+=4;
					this.knowledge += 4;
				}
				
			}else if(defenceused >=attackused && defenceused >=specialused){
				this.defence += 5;
				this.speed +=5;
				if(attackused == specialused ){
					this.attack+= 2;
					this.wisdom+=2;
					this.knowledge += 2;
				}else if(attackused >= specialused ){
					this.attack +=4;
				}else if(attackused <= specialused ){
					this.wisdom+=4;
					this.knowledge += 4;
				}
			}else if(specialused >=attackused && specialused >=defenceused){
				this.wisdom+=5;
				this.knowledge += 5;
				if(attackused == defenceused){
					this.attack+= 2;
					this.defence += 2;
					this.speed +=2;	
				}else if(attackused >= defenceused){
					this.attack+= 4;
				}else if(attackused == defenceused){
					this.defence += 4;
					this.speed +=4;	
				}
			}
			
			int tempKnowlegde = getKnowledge();
			int tempBoast = tempKnowlegde - getAttack();
			if(tempBoast > 0){
				this.totalMana += tempBoast;
				this.currentMana += tempBoast;
			}
			
			this.totalHealth += (getDefence()+ getAttack())/10;
			this.currentHealth += (getDefence()+ getAttack())/10;
		}
			
	}
	
	/**
	 * returns the creature's total max health       
	 * @return int
	 */
	public int getTotalHealth() {
		return totalHealth;
	}

	/**
	 * sets the creature's max total health     
	 * @param totalHealth of type int
	 */
	public void setTotalHealth(int totalHealth) {
		this.totalHealth = totalHealth;
	}

	/**
	 * returns the creature current health     
	 * @return int
	 */
	public int getCurrentHealth() {
		return currentHealth;
	}

	/**
	 * sets the creature's current health     
	 * @param currentHealth of type int
	 */
	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = currentHealth;
	}
	
	/**
	 * resets the creature's current health based on damage dealth    
	 * @param damage of type int
	 */
	public void damageDealt(int damage){
		this.currentHealth -=damage;
	}
	
	/**
	 * resets the creature's current health based on amount healed    
	 * @param heal of type int
	 */
	public void healDamage(int heal){
		int temp = this.currentHealth + heal;
		if(temp > totalHealth){
			currentHealth = totalHealth;
		}else if(temp <= totalHealth){
			currentHealth = temp;
		}
	}
	
	/**
	 * returns the creature's total mana    
	 * @return int
	 */
	public int getTotalMana() {
		return totalMana;
	}

	/**
	 * sets the creature total mana   
	 * @param totalMana of type int
	 */
	public void setTotalMana(int totalMana) {
		this.totalMana = totalMana;
	}

	/**
	 * return the creature's current mana    
	 * @return int
	 */
	public int getCurrentMana() {
		return currentMana;
	}

	/**
	 * sets the creature's current mana    
	 * @param currentMana of type int
	 */
	public void setCurrentMana(int currentMana) {
		this.currentMana = currentMana;
	}
	
	public void removeMana(int remove){
		this.currentMana -= remove;
	}

	/**
	 * return the creature's defence   
	 * @return int
	 */
	public int getDefence() {
		if(defending == true){
			return defence*2;
		}else{
			return defence;
		}
		
	}

	/**
	 * sets the creature's defence   
	 * @param defence of type int
	 */
	public void setDefence(int defence) {
		this.defence = defence;
	}

	/**
	 * return the creature's attack   
	 * @return int
	 */
	public int getAttack() {
		return attack;
	}

	/**
	 * sets the creature's attack   
	 * @param attack of type int
	 */
	public void setAttack(int attack) {
		this.attack = attack;
	}
	
	/**
	 * returns the creature's speed  
	 * @return int
	 */
	public int getSpeed(){
		return speed;
	}
	
	/**
	 * sets the creature's speed  
	 * @param addSpeed of type  int
	 */
	public void setSpeed(int addSpeed){
		this.speed += addSpeed;
	}
	
	
	/**
	 * return if the character is defending       
	 * @return a boolean 
	 */
	public boolean getDefending(){
		return defending; 
	}
	

	/**
	 * sets if the character is defending       
	 * @param update of type boolean 
	 */
	public void setDefending(boolean update){
		this.defending = update;
	}
	

	/**
	 * returns the character's wisdom stat       
	 * @return an integer 
	 */
	public int getWisdom() {
		return wisdom;
	}
	

	/**
	 * sets the character's wisdom stat    
	 * @param wisdom of type int 
	 */
	public void setWisdom(int wisdom) {
		this.wisdom = wisdom;
	}

	/**
	 * return the character's knowledge       
	 * @return an integer 
	 */
	public int getKnowledge() {
		return knowledge;
	}


	/**
	 * sets the character knowledge       
	 * @param knowledge of type
	 */
	public void setKnowledge(int knowledge) {
		this.knowledge = knowledge;
	}
	
	/**
	 * returns the number of times attack was used  
	 * @return int 
	 */
	public int getAttackused() {
		return attackused;
	}


	/**
	 * sets the number of times attack was used  
	 * @param attackused of type int 
	 */
	public void setAttackused(int attackused) {
		this.attackused = attackused;
	}
	
	/**
	 * increases the number of attacks used  
	 * @param plus of type int 
	 */
	public void increaseAttackused(int plus){
		this.attackused += plus;
	}
	
	/**
	 * returns the number of times defences was used  
	 * @return int 
	 */
	public int getDefenceused() {
		return defenceused;
	}
	
	/**
	 * sets the number of times defence was used  
	 * @param defenceused of type int 
	 */
	public void setDefenceused(int defenceused) {
		this.defenceused = defenceused;
	}
	
	/**
	 * increases the number of defences used  
	 * @param plus of type int 
	 */
	public void increaseDefenceused(int plus){
		this.defenceused += plus;
	}

	/**
	 * returns the number of times defence was used  
	 * @return int 
	 */
	public int getSpecialused() {
		return specialused;
	}

	/**
	 * sets the number of times special was used  
	 * @param specialused of type int 
	 */
	public void setSpecialused(int specialused) {
		this.specialused = specialused;
	}
	
	/**
	 * increases the number of specials used  
	 * @param plus of type int 
	 */
	public void increaseSpecialused(int plus){
		this.specialused += plus;
	}

	/**
	 * return a to string   
	 * @return String
	 */
	public String toString(){
		return super.toString()+" level: "+ level+" HP: " + currentHealth + " : " + totalHealth +" MP: "+ currentMana +" : "+ totalMana;
	}
}
