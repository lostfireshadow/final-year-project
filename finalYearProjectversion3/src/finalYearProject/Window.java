package finalYearProject;

import static org.lwjgl.opengl.GL11.glTexCoord2f;

import java.awt.Font;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3               
 * @since       2015-04-05         
 */

public class Window {
	 private static TrueTypeFont font;
	
	 //private static Texture testimage;
	public static void createWindow(int width, int height, String title) throws FileNotFoundException, IOException{
		Display.setTitle(title);
		
		try {
			Display.setDisplayMode(new DisplayMode(width,height));
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
		// fonts and text
		Font awtFont = new Font("Times New Roman", Font.BOLD, 18);
		font = new TrueTypeFont(awtFont, false);	
	}
	
	/**
	 * prints to screen an inputed String
	 * @param input of type String
	 * @param x of type int
	 * @param y of type int
	 */
	public static void rendertext(String input, int x, int y){
		Color.white.bind();
		font.drawString(x, y, input,Color.black);
	}
	
	/**
	 * prints to screen an inputed String
	 * @param input of type String
	 * @param x of type int
	 * @param y of type int
	 */
	public static void rendertextbattle(String input, int x, int y){
		font.drawString(x, y, input, Color.blue);
	}
	
	/**
	 * renders several of the views to the screen
	 * @param x of type int
	 * @param y of type int
	 * @param temp of type ArrayList
	 * @param area of type String
	 * @param towntest of type Texture
	 * @param cave of type Texture
	 * @param player of type Texture
	 * @param background of type Texture
	 */
	public static void render(int x, int y, ArrayList<GlobalLocation> temp, String area, Texture towntest, Texture cave, Texture player, Texture background){
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glClearColor(0, 75, 125, 255);
		for (int i = 0 ;i <temp.size();i++)
        {
       	 GlobalLocation test = temp.get(i);
       	 if(test.getQuarter().equalsIgnoreCase(area)){
	       	 	if(test.getType().equals("Town")){	 
		        	 int X = test.getPosX();
		        	 int Y = test.getPosY();
		        	 GL11.glColor3f(0.5f,0.5f,1.0f);
		        	 //GL11.glBindTexture(GL11.GL_TEXTURE_2D, testimage.getTextureID());
		        	 //town.bind();
		        	 towntest.bind();
		        	 GL11.glBegin(GL11.GL_QUADS);
		        	 glTexCoord2f(0.0f,0.0f);
		             GL11.glVertex2f(X,Y);
		             
		             glTexCoord2f(0.0f,1.0f);
		             GL11.glVertex2f(X,Y+50);
		             
		             glTexCoord2f(1.0f,1.0f);
		             GL11.glVertex2f(X+50,Y+50);
		             
		             glTexCoord2f(1.0f,0.0f);
		             GL11.glVertex2f(X+50,Y);
		             GL11.glEnd();
	       	 } else if(test.getType().equals("Dungeon")){
	       		 int X = test.getPosX();
		        	 int Y = test.getPosY();
		        	 cave.bind();
		        	 GL11.glColor3f(1.0f,0.5f,1.0f);
		        	 GL11.glBegin(GL11.GL_QUADS);
		        	 
		        	 glTexCoord2f(0,0);
		             GL11.glVertex2f(X,Y);
		             
		             glTexCoord2f(0,1);
		             GL11.glVertex2f(X,Y+50);
		             
		             glTexCoord2f(1,1);
		             GL11.glVertex2f(X+50,Y+50);
		             
		             glTexCoord2f(1,0);
		             GL11.glVertex2f(X+50,Y);
		             GL11.glEnd();
	       	 }
       	 }
        }
		//font.drawString(100, 50, "test");
       // set the color of the quad (R,G,B,A)
		GL11.glPushMatrix();
		player.bind();
		
		 GL11.glColor3f(1f,1f,1.0f);
       // draw quad
	    
       GL11.glBegin(GL11.GL_QUADS);
       glTexCoord2f(0,0);
       GL11.glVertex2f(x,y);
       glTexCoord2f(0,1);
       GL11.glVertex2f(x,y+25);
       glTexCoord2f(1,1);
       GL11.glVertex2f(x+25,y+25);
       glTexCoord2f(1,0);
       GL11.glVertex2f(x+25,y);
       GL11.glEnd();
       GL11.glPopMatrix();
       
      
	}
	
	/**
	 * renders the back ground screen that is used by several views
	 * @param background of type Texture
	 */
	public static void backgroundScreen(Texture background){
		background.bind();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		//GL11.glClearColor(0, 75, 125, 255);
		
		GL11.glPushMatrix();
        GL11.glColor4f(1.0f,1.0f,1.0f,1.0f);
    	GL11.glBegin(GL11.GL_QUADS);
    	glTexCoord2f(0,0);
    	GL11.glVertex2f(50,50);
    	
    	glTexCoord2f(0,1);
		GL11.glVertex2f(550,50);
		
		glTexCoord2f(1,1);
		GL11.glVertex2f(550,550);
		glTexCoord2f(1,0);
		GL11.glVertex2f(50,550);
    	GL11.glEnd();
        GL11.glPopMatrix();	
	}
	
	/**
	 * renders several of the views to the screen
	 * @param menuNeeded of type boolean
	 * @param playerStatsNeeded of type boolean
	 * @param inventoryNeeded of type boolean
	 * @param symbol of type Texture
	 * @param background of type Texture
	 */
	public static void renderMenu(boolean menuNeeded, boolean playerStatsNeeded, boolean inventoryNeeded, Texture symbol,Texture background){
		 // the menu
	       if(menuNeeded == true){
	    	   if(playerStatsNeeded == true || inventoryNeeded == true){
	    		   backgroundScreen(background);
	           }
	       	GL11.glPushMatrix();
	       	GL11.glColor3f(0.5f,0.5f,0.5f);
	       	GL11.glBegin(GL11.GL_QUADS);
	       	GL11.glVertex2f(600,00);
	   		GL11.glVertex2f(600+200,00);
	   		GL11.glVertex2f(600+200,600+200);
	   		GL11.glVertex2f(600,600+200);
	       	GL11.glEnd();
	        GL11.glPopMatrix();
	        
	        GL11.glPushMatrix();
	       	GL11.glColor3f(0.5f,1f,1.0f);
	       	GL11.glBegin(GL11.GL_QUADS);
	       	GL11.glVertex2f(625,450);
	   		GL11.glVertex2f(625+150,450);
	   		GL11.glVertex2f(625+150,450+50);
	   		GL11.glVertex2f(625,450+50);
	       	GL11.glEnd();
	        GL11.glPopMatrix();
	        
	        GL11.glPushMatrix();
	       	GL11.glColor3f(0.5f,1f,1.0f);
	       	GL11.glBegin(GL11.GL_QUADS);
	       	GL11.glVertex2f(625,350);
	   		GL11.glVertex2f(625+150,350);
	   		GL11.glVertex2f(625+150,350+50);
	   		GL11.glVertex2f(625,350+50);
	       	GL11.glEnd();
	        GL11.glPopMatrix();
	        
	        GL11.glPushMatrix();
	       	GL11.glColor3f(0.5f,1f,1.0f);
	       	GL11.glBegin(GL11.GL_QUADS);
	       	GL11.glVertex2f(625,250);
	   		GL11.glVertex2f(625+150,250);
	   		GL11.glVertex2f(625+150,250+50);
	   		GL11.glVertex2f(625,250+50);
	       	GL11.glEnd();
	        GL11.glPopMatrix();  
	       }
	}
	/**
	 * renders the main menu
	 * @param background of type Texture
	 */
	public static void renderMainMenuText(Texture background){
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		background.bind();
		GL11.glPushMatrix();
		GL11.glColor4f(1.0f,1.0f,1.0f,1.0f);
	    GL11.glBegin(GL11.GL_QUADS);
	    glTexCoord2f(0,0);
	    GL11.glVertex2f(350,150);
	    glTexCoord2f(0,1);
	    GL11.glVertex2f(400,150);
	    glTexCoord2f(1,1);
	    GL11.glVertex2f(400,200);
	    glTexCoord2f(1,0);
	    GL11.glVertex2f(350,200);
	    GL11.glEnd();
		GL11.glPopMatrix();	
	}
	
	/**
	 * renders the shop view
	 * @param background of type Texture
	 * @param towntest of type Texture
	 */
	public static void renderShop(Texture background, Texture towntest){
		background.bind();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		//GL11.glClearColor(0, 75, 125, 255);
		
		GL11.glPushMatrix();
        GL11.glColor4f(1.0f,1.0f,1.0f,1.0f);
    	GL11.glBegin(GL11.GL_QUADS);
    	glTexCoord2f(0,0);
    	GL11.glVertex2f(50,50);
    	
    	glTexCoord2f(0,1);
		GL11.glVertex2f(750,50);
		
		glTexCoord2f(1,1);
		GL11.glVertex2f(750,550);
		glTexCoord2f(1,0);
		GL11.glVertex2f(50,550);
    	GL11.glEnd();
        GL11.glPopMatrix();	
        
        renderShopItem( towntest);
        
        
	}
	/**
	 * renders the shop item 
	 * @param towntest of type Texture
	 */
	public static void renderShopItem(Texture towntest){
		towntest.bind();
        GL11.glPushMatrix();
		GL11.glColor4f(1.0f,1.0f,1.0f,1.0f);
	    GL11.glBegin(GL11.GL_QUADS);
	    glTexCoord2f(0,0);
	    GL11.glVertex2f(350,150);
	    glTexCoord2f(0,1);
	    GL11.glVertex2f(400,150);
	    glTexCoord2f(1,1);
	    GL11.glVertex2f(400,200);
	    glTexCoord2f(1,0);
	    GL11.glVertex2f(350,200);
	    GL11.glEnd();
		GL11.glPopMatrix();	
	}
	
	/**
	 * renders the battle view
	 * @param temp of type ArrayList
	 * @param background of type Texture
	 * @param stickmen of type Texture
	 * @param bigBad of type Texture
	 */
	public static void renderBattle(ArrayList<Mob> temp, Texture background, Texture stickmen, Texture bigBad){
		background.bind();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		//GL11.glClearColor(0, 75, 125, 255);
		
		GL11.glPushMatrix();
        GL11.glColor4f(1.0f,1.0f,1.0f,1.0f);
    	GL11.glBegin(GL11.GL_QUADS);
    	glTexCoord2f(0,0);
    	GL11.glVertex2f(50,50);
    	
    	glTexCoord2f(0,1);
		GL11.glVertex2f(750,50);
		
		glTexCoord2f(1,1);
		GL11.glVertex2f(750,550);
		glTexCoord2f(1,0);
		GL11.glVertex2f(50,550);
    	GL11.glEnd();
        GL11.glPopMatrix();
        
        int posx = 150;
        int posy= 400;
        
        for(int i = 0; i < temp.size(); i++){
        	if(temp.get(i).getName().contains("Captian imp")){
		        bigBad.bind();
		        GL11.glPushMatrix();
				GL11.glColor4f(1.0f,1.0f,1.0f,1.0f);
			    GL11.glBegin(GL11.GL_QUADS);
			    glTexCoord2f(0,0);
			    GL11.glVertex2f(posx,posy);
			    glTexCoord2f(0,1);
			    GL11.glVertex2f(posx+100,posy);
			    glTexCoord2f(1,1);
			    GL11.glVertex2f(posx+100,posy+100);
			    glTexCoord2f(1,0);
			    GL11.glVertex2f(posx,posy+100);
			    GL11.glEnd();
				GL11.glPopMatrix();	
				posx+=75;
        	}else{
        		stickmen.bind();
		        GL11.glPushMatrix();
				GL11.glColor4f(1.0f,1.0f,1.0f,1.0f);
			    GL11.glBegin(GL11.GL_QUADS);
			    glTexCoord2f(0,0);
			    GL11.glVertex2f(posx,posy);
			    glTexCoord2f(0,1);
			    GL11.glVertex2f(posx+100,posy);
			    glTexCoord2f(1,1);
			    GL11.glVertex2f(posx+100,posy+100);
			    glTexCoord2f(1,0);
			    GL11.glVertex2f(posx,posy+100);
			    GL11.glEnd();
				GL11.glPopMatrix();	
				posx+=75;
        	}
        }
		 GL11.glPushMatrix();
			GL11.glColor3f(1.0f, 0.0f ,1.0f);
		    GL11.glBegin(GL11.GL_QUADS);
		    GL11.glVertex2f(675,400);
		    GL11.glVertex2f(750,400);
		    GL11.glVertex2f(750,450);
		    GL11.glVertex2f(675,450);
		    GL11.glEnd();
		GL11.glPopMatrix();	
		
		
		GL11.glPushMatrix();
		GL11.glColor3f(1.0f, 0.0f ,1.0f);
	    GL11.glBegin(GL11.GL_QUADS);
	    GL11.glVertex2f(675,300);
	    GL11.glVertex2f(750,300);
	    GL11.glVertex2f(750,350);
	    GL11.glVertex2f(675,350);
	    GL11.glEnd();
	GL11.glPopMatrix();	
	
	GL11.glPushMatrix();
	GL11.glColor3f(1.0f, 0.0f ,1.0f);
    GL11.glBegin(GL11.GL_QUADS);
    GL11.glVertex2f(675,200);
    GL11.glVertex2f(750,200);
    GL11.glVertex2f(750,250);
    GL11.glVertex2f(675,250);
    GL11.glEnd();
GL11.glPopMatrix();	
        
	}
	
	/**
	 * renders the battle view
	 * @param background of type Texture
	 * @param height of type int
	 * @param width of type int
	 * @param xPos of type int
	 * @param yPos of type int
	 * @param player of type Texture
	 */
	public static void renderDungeon(Texture background, int height, int width, int xPos, int yPos, Texture player, Dungeon temp,Texture exit){
		GL11.glPushMatrix();
		GL11.glColor4f(1.0f,1.0f,1.0f,1.0f);
		background.bind();
	    GL11.glBegin(GL11.GL_QUADS);
	    glTexCoord2f(0,0);
	    GL11.glVertex2f(0,0);
	    glTexCoord2f(0,1);
	    GL11.glVertex2f(width,0);
	    glTexCoord2f(1,1);
	    GL11.glVertex2f(width,height);
	    glTexCoord2f(1,0);
	    GL11.glVertex2f(0,height);
	    GL11.glEnd();
	    GL11.glPopMatrix();
	    
	    
	    GL11.glPushMatrix();
	    exit.bind();
		   GL11.glColor3f(1f,1f,1.0f);
	       GL11.glBegin(GL11.GL_QUADS);
	       glTexCoord2f(0,0);
	       GL11.glVertex2f(0,0);
	       glTexCoord2f(0,1);
	       GL11.glVertex2f(0,50);
	       glTexCoord2f(1,1);
	       GL11.glVertex2f(50,50);
	       glTexCoord2f(1,0);
	       GL11.glVertex2f(50,0);
	       GL11.glEnd();
	       GL11.glPopMatrix();
	       
	   GL11.glPushMatrix();
	   player.bind();
	   GL11.glColor3f(1f,1f,1.0f);
       GL11.glBegin(GL11.GL_QUADS);
       glTexCoord2f(0,0);
       GL11.glVertex2f(xPos,yPos);
       glTexCoord2f(0,1);
       GL11.glVertex2f(xPos,yPos+25);
       glTexCoord2f(1,1);
       GL11.glVertex2f(xPos+25,yPos+25);
       glTexCoord2f(1,0);
       GL11.glVertex2f(xPos+25,yPos);
       GL11.glEnd();
       GL11.glPopMatrix();
 	//TODO add exit point and set up so that party can move around, a maze like room
       //System.out.println(temp.getChests().size());
       for(int i = 0; i < temp.getChests().size();i++){
    	   Chest cTemp = temp.getChestI(i);
    	   int posX = cTemp.getPosX();
    	   int posY = cTemp.getPosY();
    	   GL11.glPushMatrix();
		   GL11.glColor3f(1f,0.5f,1.0f);
	       GL11.glBegin(GL11.GL_QUADS);
	       //glTexCoord2f(0,0);
	       GL11.glVertex2f(posX,posY);
	       //glTexCoord2f(0,1);
	       GL11.glVertex2f(posX,posY+25);
	       //glTexCoord2f(1,1);
	       GL11.glVertex2f(posX+25,posY+25);
	       //glTexCoord2f(1,0);
	       GL11.glVertex2f(posX+25,posY);
	       GL11.glEnd();
	       GL11.glPopMatrix();
       }
		
	}
	
	/**
	 * renders inputed textures in a given position and as a given size square
	 * @param texture of type Texture
	 * @param Xpos of type int
	 * @param Ypos of type int
	 * @param size of type int
	 */
	public static void renderImage(Texture texture, int Xpos, int Ypos, int size){
		GL11.glPushMatrix();
			GL11.glColor4f(1.0f,1.0f,1.0f,1.0f);
		    texture.bind();
		    GL11.glBegin(GL11.GL_QUADS);
		    glTexCoord2f(0,0);
		    GL11.glVertex2f(Xpos,Ypos);
		    glTexCoord2f(0,1);
		    GL11.glVertex2f(Xpos+size,Ypos);
		    glTexCoord2f(1,1);
		    GL11.glVertex2f(Xpos+size,Ypos+size);
		    glTexCoord2f(1,0);
		    GL11.glVertex2f(Xpos,Ypos+size);
		    GL11.glEnd();
		GL11.glPopMatrix();	
	}
	
	public static boolean isCloseRequested(){
		return Display.isCloseRequested();
	}
	
	public static int getWidth(){
		return Display.getDisplayMode().getWidth();
	}
	
	public static int getHeight(){
		return Display.getDisplayMode().getHeight();
	}
	
	public static String getTitle(){
		return Display.getTitle();
	}
	
}
