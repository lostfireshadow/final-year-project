package finalYearProject;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Entity {
	private String name;
	
	public Entity(String entreeName){
		this.name = entreeName;
	}
	
	/**
	 * sets the entity's name
	 * @param neoName of type String
	 */ 
	public void setName(String neoName){
		this.name = neoName;
	}
	

	/**
	 * returns the entity's name
	 * @return String
	 */ 
	public String getName(){
		return name;
	}
	
	/**
	 * returns a toString about the entity
	 * @return String
	 */ 
	public String toString(){
		return"Name: "+ name;
	}

}
