package finalYearProject;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Item {
	private String name;
	private int total;
	private int cost;
	
	public Item(String deName, int deCost){
		this.name = deName;
		this.cost = deCost;
	}
	
	/**
	 * returns the item's name
	 * @return String
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * sets the item's name
	 * @param name of type String
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * sets the item's namecost
	 * @param deCost of type int
	 */
	public void setCost(int deCost){
		this.cost = deCost;
	}
	
	/**
	 * returns the item's cost
	 * @return int
	 */
	public int getCost(){
		return cost;
	}
	
	/**
	 * set  the total number item/ the number of uses left
	 * @param i of type int
	 */
	public void setTotal(int i){
		this.total= i;
	}
	
	/**
	 * returns the number of uses left
	 * @return int
	 */
	public int getTotal(){
		return total;
	}

}
