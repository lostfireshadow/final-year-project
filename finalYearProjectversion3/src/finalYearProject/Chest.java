package finalYearProject;
/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-03-30          
 */
public class Chest {
	private boolean isOpened;
	private int posX;
	private int posY;
	private String contains;
	private int numberOf;
	
	public Chest(int neoXPos, int neoYPos, String deContains, int deNumberOf){
		this.posX = neoXPos;
		this.posY = neoYPos;
		this.contains = deContains;
		this.numberOf = deNumberOf;
		this.isOpened = false;
	}
	
	/**
	 * returns the X position  
	 * @return int
	 */
	public int getPosX(){
		return posX;
	}
	
	/**
	 * sets the x position  
	 * @param neoPosX of type int
	 */
	public void setPosX(int neoPosX){
		this.posX = neoPosX;
	}
	
	/**
	 * returns the Y position  
	 * @return int
	 */
	public int getPosY(){
		return posY;
	}
	
	/**
	 * sets the y position  
	 * @param neoPosY of type int
	 */
	public void setPosY(int neoPosY){
		this.posY = neoPosY;
	}
	
	/**
	 * returns the contains of the chest  
	 * @return String
	 */
	public String getContains(){
		return contains;
	}
	
	/**
	 * sets what is contained in the chest  
	 * @param neoContains of type String
	 */
	public void setContains(String neoContains){
		this.contains = neoContains;
	}
	
	/**
	 * returns the number of objects in the chest 
	 * @return int
	 */
	public int getNumberOf(){
		return numberOf;
	}
	
	/**
	 * sets the number of objects contained in the chest  
	 * @param neoNumberOf of type int
	 */
	public void setNumberof(int neoNumberOf){
		this.numberOf = neoNumberOf;
	}
	
	/**
	 * returns if the chest ahd been opened 
	 * @return boolean
	 */
	public boolean getIsOpened(){
		return isOpened;
	}
	
	/**
	 * sets if the chest had been opened 
	 * @param input of type boolean
	 */
	public void setIsOpened(boolean input){
		this.isOpened = input;
	}

}
