package finalYearProject;

import java.util.ArrayList;
/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-03-30          
 */

public class Character extends Creature{
	private ArrayList<Item> inventory;
	private ArrayList<Clothes> clothes;
	private int experience;
	private int nextLevel;
	public Character(String entreeName, int level, int totalHealth,
			int currentHealth, int totalMana, int currentMana, int defence, int attack) {
		super(entreeName, level, totalHealth, currentHealth, totalMana,currentMana, defence, attack);
		inventory = new ArrayList<Item>();
		clothes = new ArrayList<Clothes>();
		this.experience = 0;
		this.nextLevel = 50;
		
	}
	
	/**
	 * adds a clothes object to the arraylist clothes                        
	 *  @param neoClothes of Type Clothes         
	 * 
	 */
	public void addClothes(Clothes neoClothes){
		clothes.add(neoClothes);
	}
	

	/**
	 * returns object at the given position of the clothes array list                      
	 * @param i of type int 
	 * @return Clothes        
	 * 
	 */
	public Clothes getIthClothes(int i){
		return clothes.get(i);
	}
	
	/**
	 * adds a item object to the array list inventory                        
	 *  @param neoItem of Type Item         
	 * 
	 */
	public void addItem(Item neoItem){
		inventory.add(neoItem);
	}
	
	/**
	 * returns object at the given position of the inventory array list                      
	 * @param i of type int  
	 * @return item       
	 * 
	 */
	public Item getithItem(int i){
		return inventory.get(i);
	}
	
	/**
	 * returns the inventory arraylist 
	 * @return ArrayList                       
	 */
	public ArrayList<Item> getInventory(){
		return inventory;  
	}
	
	/**
	 * returns the experince of the character
	 * @return int                            
	 */
	public int getExperience(){
		return experience;
	}
	
	/**
	 * sets the character experience to certain amount                   
	 * @param expTotal of type int        
	 * 
	 */
	public void setExp(int expTotal){
		this.experience = expTotal;
	}
	
	/**
	 * adds to the experience and sees if the character can level up                     
	 * @param exp of type int        
	 * 
	 */
	public void addExp(int exp){
	    int neoExp = getExperience() + exp;
	    this.experience = neoExp;
	    boolean LeveledUP = false;
	    while(experience >= nextLevel){
	    	int temp = nextLevel;
			int addednew =nextLevel/2;
			this.nextLevel = temp+addednew;
			LevelUpAffected();
	    }
	    if(LeveledUP == true){
	    	setAttackused(0);
	    	setDefenceused(0);
	    	setSpecialused(0);
	    }
	}
	/**
	 * returns is the character can level up i.e. improve it stats
	 * @return  boolean                     
	 */
	public boolean ifcanLP() {
		int currentExp = experience;
		if(currentExp >= nextLevel){
			int temp = nextLevel;
			int addednew =nextLevel/2;
			this.nextLevel = temp+addednew;
			return true;
		}
		return false;
	}
	
	/**
	 * checks to see if the character can level up i.e. improve it stats
	 * @param experince                      
	 */
	public void setNextLevel(int experince){
		 while(experince >= nextLevel){
			 int temp = nextLevel/2;
			 this.nextLevel +=temp;
		 }
	 }
	public String toString(){
		return super.toString();
	}

}
