package finalYearProject;
import java.util.ArrayList;

/**
 * @author      Ruairi Whelan      ruairimwhelan@gmail.com
 * @version     1.3                
 * @since       2015-04-05         
 */

public class Party {
	private ArrayList <Character> deParty;
	private ArrayList <Item> partyInventory;
	private int money;
	private int yinYang;
	
	public Party(){
		this.deParty = new ArrayList <Character>();
		this.partyInventory = new ArrayList <Item>();
		this.money = 0;
		this.yinYang = 0;
	}
	
	/**
	 * return a list of of the party members      
	 * @return ArrayList
	 */
	public ArrayList<Character> getParty(){
		return deParty;
	}
	
	/**
	 * returns a list of the Items in the party inventory     
	 * @return ArrayList
	 */
	public ArrayList<Item> getPI(){ // get party Inventory
		return partyInventory;
	}
	
	/**
	 * adds a new member to the party     
	 * @param testee of type character  
	 */
	public void newCharacter(Character testee){
		deParty.add(testee);
	}
	
	/**
	 * returns the character stored in the i'th position of the party ArrayList    
	 * @param i of type int
	 * @return character   
	 */
	public Character getCharacter(int i){
		return deParty.get(i);
	}
	
	/**
	 * adds a new item to the inventory     
	 * @param deItem of type item  
	 */
	public void newItem(Item deItem){
		partyInventory.add(deItem);
	}
	
	/**
	 * returns the Item stored in the i'th position of the inventory ArrayList    
	 * @param i of type int
	 * @return Item   
	 */
	public Item getItem(int i){
		return partyInventory.get(i);
	}
	
	/**
	 * sets the total amount of money the party has    
	 * @param neoMoney of type int  
	 */
	public void setmoney(int neoMoney){
		this.money = neoMoney;
	}
	
	/**
	 * adds to the total amount of money the party has    
	 * @param neoMoney of type int  
	 */
	public void addMoney(int neoMoney){
		this.money+= neoMoney;
	}
	
	/**
	 * removes from the total amount of money the party has    
	 * @param cost of type int  
	 */
	public void removeMoney(int cost){
		this.money -=cost;
	}
	/**
	 * return the total amount of money the party has  
	 * @return int 
	 */
	public int getMoney(){
		return money;
	}
	
	/**
	 * return the party's yinYang value  
	 * @return int 
	 */
	public int getYinYang(){
		return yinYang;
	}
	/**
	 * sets  the party's yinYang value  
	 * @param balance of type int
	 */
	
	public void setYinYang(int balance){
		this.yinYang = balance;
	}
	
	/**
	 * increases party yinYang value by one  
	 */
	public void increaseYanYang(){
		this.yinYang += 1;
	}
	
	/**
	 * decreases party yinYang value by one  
	 */
	public void decreaseYanYang(){
		this.yinYang -= 1;
	}

}
