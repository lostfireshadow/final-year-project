package finalYearProjecttest;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import finalYearProject.Clothes;

public class ClothesTest {
	Clothes top = new Clothes("Styling Top",2500, "the most Styling Top", 45,"light");

	@Test
	public void testgetter() {
		assertEquals("Styling Top",top.getName());
		assertEquals("the most Styling Top",top.getDescribe());
	}
	
	@Test
	public void testsetters() {
		top.setName("Neo Top");
		top.setDescribe("the post modren top");
		assertEquals("Neo Top",top.getName());
		assertEquals("the post modren top",top.getDescribe());
	}

}
