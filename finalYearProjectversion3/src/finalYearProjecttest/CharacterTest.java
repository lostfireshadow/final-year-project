package finalYearProjecttest;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import finalYearProject.Character;
import finalYearProject.Clothes;
import finalYearProject.Item;

public class CharacterTest {
                                         // name, health, cur health, mana, cur mana, defence, attack
	Character character1 = new Character("Test case 1", 21, 1234,1234,500, 500, 50, 30);
	Item newItem = new Item("bottle",25);
	Clothes neoClothes = new Clothes("ragged top",150, "a ragged old top",1,"light");
	@Test
	public void testReturns() {
		assertEquals("Test case 1", character1.getName());
		assertEquals(21, character1.getLevel());
		assertEquals(1234, character1.getTotalHealth());
		assertEquals(1234, character1.getCurrentHealth());
		assertEquals(500, character1.getTotalMana());
		assertEquals(500, character1.getCurrentMana());
		assertEquals(50, character1.getDefence());
		assertEquals(30, character1.getAttack());
	}
	
	@Test
	public void testSetter(){
		character1.setLevel(22);
		assertEquals(22, character1.getLevel());
		character1.levelup();
		assertEquals(23, character1.getLevel());
		character1.setLevel(21);
		character1.setCurrentMana(300);
		assertEquals(505, character1.getTotalMana());
		assertEquals(300, character1.getCurrentMana());
	}
	
	@Test 
	public void testArrayList(){
		character1.addItem(newItem);
		character1.addClothes(neoClothes);
		assertEquals("bottle", character1.getithItem(0).getName());
		assertEquals("ragged top", character1.getIthClothes(0).getName());
		assertEquals("a ragged old top", character1.getIthClothes(0).getDescribe());
	}
	
	@Test 
	public void setlevel(){
		Character testChar =new Character("tim", 1, 50,50, 10, 10, 5,5);
		assertEquals(4,testChar.levelgenetor(135));
		assertEquals(11,testChar.getDefence());
	}

}
