package finalYearProjecttest;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import finalYearProject.GlobalLocation;

public class GlobalLocationtest {

	GlobalLocation myTest = new GlobalLocation("point 1", 30, 21, "middle of noWhere", "test1","south");

	@Test
	public void testGetter() {
		assertEquals("point 1",myTest.getName());
		assertEquals(30,myTest.getPosX());
		assertEquals(21,myTest.getPosY());
		assertEquals("middle of noWhere",myTest.getData());
		assertEquals("test1",myTest.getType());
	}
	
	@Test
	public void testSetter() {
		myTest.setName("point2");
		assertEquals("point2",myTest.getName());
		myTest.setPosX(44);
		assertEquals(44,myTest.getPosX());
		myTest.setPosY(66);
		assertEquals(66,myTest.getPosY());
		myTest.setData("it is a test, only a test");
		assertEquals("it is a test, only a test",myTest.getData());
		myTest.setType("testing you");
		assertEquals("testing you",myTest.getType());
	}

}
