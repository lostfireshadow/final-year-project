package finalYearProjecttest;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import finalYearProject.MedicalItem;

public class medicalItemTest {

	MedicalItem elixar = new MedicalItem("elixar", 1500, false, 300);

	@Test
	public void testGetters() {
		assertEquals("elixar", elixar.getName());
		assertEquals(false, elixar.getIsHealth());
		assertEquals(300, elixar.getAmount());
	}
	
	@Test
	public void testSetters() {
		elixar.setName("test name");
		assertEquals("test name", elixar.getName());
		elixar.setHealth(true);
		assertEquals(true, elixar.getIsHealth());
		elixar.setAmount(500);
		assertEquals(500, elixar.getAmount());
	}

}
