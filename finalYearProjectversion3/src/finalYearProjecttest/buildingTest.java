package finalYearProjecttest;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import finalYearProject.Building;

public class buildingTest {

	Building building = new Building(5,4);

	@Test
	public void testGetters() {
		assertEquals(5, building.getPosX());
		assertEquals(4, building.getPosY());
	}
	
	@Test
	public void testSetters() {
		building.setPosX(10);
		assertEquals(10, building.getPosX());
		building.setPosY(10);
		assertEquals(10, building.getPosY());
	}

}
