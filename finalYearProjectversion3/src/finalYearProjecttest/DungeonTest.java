package finalYearProjecttest;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import finalYearProject.Chest;
import finalYearProject.Dungeon;

public class DungeonTest {

	Dungeon cave =  new Dungeon("Big Cave", 25, 40, "A large Cave","Dungeon", 3,"south");
	String monster1 = "it's monster 1";
	Chest chest1 = new Chest(0,0,"nothing special", 0);
	@Test
	public void testgetters() {
		assertEquals("Big Cave", cave.getName());
		assertEquals(25, cave.getPosX());
		assertEquals(3, cave.getRecLevel());
	}
	
	@Test
	public void testSetters(){
		cave.setRecLevel(5);
		assertEquals(5, cave.getRecLevel());
	}
	
	@Test
	public void testArray(){
		cave.addChest(chest1);
		assertEquals(1, cave.getChests().size());
		cave.addMonster(monster1);
		assertEquals("it's monster 1", cave.getMonsterI(0));
	}

}
