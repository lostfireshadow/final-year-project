package finalYearProjecttest;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import finalYearProject.Item;

public class ItemTest {

	Item item1 = new Item("empty jar",35);
	Item item2 = new Item("glass Bottle",5);
	@Test
	public void testgetter() {
		assertEquals("empty jar",item1.getName());
		assertEquals("glass Bottle",item2.getName());
	}
	
	@Test
	public void testSetter(){
		item1.setName("New Item Test");
		assertEquals("New Item Test",item1.getName());
	}

}
