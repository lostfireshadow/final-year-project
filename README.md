# README #
this the README is for my final year project, currently called the game of life and mystery

### What is this repository for? ###

the project is based on the older style of Role Playing Game(rpg), it unique aspect is the stat gaining system, which is based on the players actions and the AI system which is also affect by the players actions.

the project is currently in its third iterations, note that the iteration 1 and 2, were not developed using bit bucket as a repo, however they have being pushed onto the repo

### How do I get set up? ###

** Summary of set up**

inorder to set up the project, using eclipse, just move the project folder in to your workspace and import the project into eclipse. you will have to set up the libraries, but this done the same way as another external jar file, just make sure that it the Slick-Util.jar and the lwjgl2.9.1.jar. 

also lwjgl2.9.1.jar will require you to specify it native, this is the operating system your are using, this folder is in the native folder for the lwjgl2.9.1 folder stucture.

the only Dependencies are slick-Util and lwjgl. 
note the Slick-util folder has a lwjgl2.9.1.jar, and using this jar will make the program work, in theory, I only used the jar on a small test project.
the reason that lwjgl folder is include is due to the native issue.

** How to run tests**

the project is a gradle project tests can be run automatically when the option as a gradle build, the linking to the testing package is already done. 

### Who do I talk to? ###

 Ruairi whelan email address: ruairimwhelan@gmail.com. I'm the admin and sole prgrammer on this project